//
//  KBSpinner.h
//  WaterBottle
//
//  Created by Kashyap Bhatt on 22/06/19.
//  Copyright © 2019 Kashyap. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface KBSpinner : NSObject

/*
 * shows the activity indication on the top window & start circular animation
 */
+ (void)show;


/*
 * Stops animation & dismiss the activity indication is any available
 */
+ (void)dismiss;

@end

NS_ASSUME_NONNULL_END
