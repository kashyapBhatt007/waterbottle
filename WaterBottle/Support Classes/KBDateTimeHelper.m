//
//  KBDateTimeHelper.m
//  WaterBottle
//
//  Created by Kashyap Bhatt on 17/08/19.
//  Copyright © 2019 Kashyap. All rights reserved.
//

#import "KBDateTimeHelper.h"

@implementation KBDateTimeHelper

+(NSString *)ConvertDate:(NSDate *)date ToFormat:(NSString *)Format
{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:Format];
    
    return [dateFormatter stringFromDate:date];
}
@end
