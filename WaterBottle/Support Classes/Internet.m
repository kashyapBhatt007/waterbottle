//
//  Internet.m
//  ToolytSFA-iOS
//
//  Created by Kashyap Bhatt on 18/05/17.
//  Copyright © 2017 toolyt. All rights reserved.
//

#import "Internet.h"
#import "Reachability.h"

@implementation Internet

// to check if Internet is available or not
+(NSString *)CheckInternet
{
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus internetStatus = [reachability currentReachabilityStatus];
    NSString * internet;
    if (internetStatus != NotReachable)
    {
        internet=@"Yes";
    }
    else {
        internet=@"No";
    }
    return internet;
}

@end
