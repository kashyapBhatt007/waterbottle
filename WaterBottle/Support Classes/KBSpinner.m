//
//  KBSpinner.m
//  WaterBottle
//
//  Created by Kashyap Bhatt on 22/06/19.
//  Copyright © 2019 Kashyap. All rights reserved.
//

#import "KBSpinner.h"
#import <UIKit/UIKit.h>

@implementation KBSpinner
{
    UIView * backViewForSpinner;
}

+ (KBSpinner *) instance
{
    static KBSpinner *instance = nil;
    static dispatch_once_t oncePredicate;
    dispatch_once(&oncePredicate, ^{
        instance = [[self alloc] init];
    });
    
    return instance;
}

+ (void)show
{
    [[KBSpinner instance] show];
}

- (void)show
{
    if(backViewForSpinner)
        return;
    float width = [[UIScreen mainScreen] bounds].size.width;
    float height = [[UIScreen mainScreen] bounds].size.height;
    
    backViewForSpinner=[[UIView alloc]init];
    backViewForSpinner.frame= CGRectMake(width/2-25, height/2-25, 50, 50);
    //backViewForSpinner.backgroundColor=[UIColor lightGrayColor];
    backViewForSpinner.layer.cornerRadius=5;
    backViewForSpinner.clipsToBounds=YES;
    backViewForSpinner.hidden=NO;
    
    CAGradientLayer *gradient = [CAGradientLayer layer];

    gradient.frame = backViewForSpinner.bounds;
    gradient.colors = [NSArray arrayWithObjects:
                       (id)[UIColor colorWithRed:52.0f/255.0f green:37.0f/255.0f blue:175.0f/255.0f alpha:1.0f].CGColor,
                       (id)[UIColor colorWithRed:3.0f/255.0f green:110.0f/255.0f blue:216.0f/255.0f alpha:1.0f].CGColor,
                       nil];
    
    [backViewForSpinner.layer insertSublayer:gradient atIndex:0];
    
    UIActivityIndicatorView * spinner = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhite];
    spinner.center = CGPointMake(160, 240);
    spinner.frame = CGRectMake(0,0, 50, 50);
    spinner.tag = 12;
    [backViewForSpinner addSubview:spinner];
    [spinner startAnimating];

    NSEnumerator *frontToBackWindows = [UIApplication.sharedApplication.windows reverseObjectEnumerator];
    for (UIWindow *window in frontToBackWindows){
        BOOL windowOnMainScreen = window.screen == UIScreen.mainScreen;
        BOOL windowIsVisible = !window.hidden && window.alpha > 0;
        BOOL windowLevelNormal = window.windowLevel == UIWindowLevelNormal;
        
        if (windowOnMainScreen && windowIsVisible && windowLevelNormal) {
            [window addSubview:backViewForSpinner];
            break;
        }
    }
    
    [spinner startAnimating];
}

+ (void)dismiss
{
    [[KBSpinner instance] dismiss];
}

- (void)dismiss
{
    if(backViewForSpinner)
    {
        backViewForSpinner.hidden=YES;
        backViewForSpinner = nil;
    }
}

@end
