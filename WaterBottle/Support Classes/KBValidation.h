//
//  KBValidation.h
//  WaterBottle
//
//  Created by Kashyap Bhatt on 22/06/19.
//  Copyright © 2019 Kashyap. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface KBValidation : NSObject

+(BOOL) NSStringIsValidEmail:(NSString *)checkString;
+(BOOL) NSStringIsValidURL: (NSString *) checkString;

@end

NS_ASSUME_NONNULL_END
