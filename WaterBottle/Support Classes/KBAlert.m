//
//  KBAlert.m
//  WaterBottle
//
//  Created by Kashyap Bhatt on 23/06/19.
//  Copyright © 2019 Kashyap. All rights reserved.
//

#import "KBAlert.h"
#import <FTIndicator/FTToastIndicator.h>

@implementation KBAlert

+(void)ShowToast:(NSString *)Title :(NSString *)Message
{
    NSString * makeMessage;
    
    if (![Title isEqualToString:@""] && ![Message isEqualToString:@""])
    {
        makeMessage=[[NSString alloc]initWithFormat:@"%@\n%@",Title,Message];
    }
    else if ([Title isEqualToString:@""] && ![Message isEqualToString:@""])
    {
        makeMessage=[[NSString alloc]initWithFormat:@"%@",Message];
    }
    else if (![Title isEqualToString:@""] && [Message isEqualToString:@""])
    {
        makeMessage=[[NSString alloc]initWithFormat:@"%@",Title];
    }
    
    [FTToastIndicator setToastIndicatorStyle:UIBlurEffectStyleDark];
    [FTToastIndicator showToastMessage:makeMessage];
}

+(void)ShowInternetToast
{
    [FTToastIndicator setToastIndicatorStyle:UIBlurEffectStyleDark];
    [FTToastIndicator showToastMessage:@"No Internet Connection"];
}

+(void)ShowAlertWithTitle:(NSString *)title andMessage:(NSString *)message
{
    UIAlertController * alert=[UIAlertController alertControllerWithTitle:title message:message preferredStyle:UIAlertControllerStyleAlert];
    
    [alert addAction:[UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleDefault handler:nil]];
    
    UIViewController * topViewController=[UIApplication sharedApplication].keyWindow.rootViewController;
    
    [topViewController presentViewController:alert animated:true completion:nil];
}

@end
