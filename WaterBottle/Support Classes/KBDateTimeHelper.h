//
//  KBDateTimeHelper.h
//  WaterBottle
//
//  Created by Kashyap Bhatt on 17/08/19.
//  Copyright © 2019 Kashyap. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface KBDateTimeHelper : NSObject

+(NSString *)ConvertDate:(NSDate *)date ToFormat:(NSString *)Format;
@end

NS_ASSUME_NONNULL_END
