//
//  Internet.h
//  ToolytSFA-iOS
//
//  Created by Kashyap Bhatt on 18/05/17.
//  Copyright © 2017 toolyt. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Internet : NSObject

+(NSString *)CheckInternet;

@end
