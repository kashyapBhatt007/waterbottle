//
//  KBAlert.h
//  WaterBottle
//
//  Created by Kashyap Bhatt on 23/06/19.
//  Copyright © 2019 Kashyap. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface KBAlert : NSObject

+(void)ShowInternetToast;

+(void)ShowToast:(NSString *)Title :(NSString *)Message;

+(void)ShowAlertWithTitle:(NSString *)title andMessage:(NSString *)message;
@end

NS_ASSUME_NONNULL_END
