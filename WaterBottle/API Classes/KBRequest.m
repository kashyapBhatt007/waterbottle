//
//  KBRequest.m
//  WaterBottle
//
//  Created by Kashyap Bhatt on 22/06/19.
//  Copyright © 2019 Kashyap. All rights reserved.
//

#import "KBRequest.h"
#import "StaticKeys.h"

@implementation KBRequest

/*
 * GET request
 */
+ (void)GETRequestToURL:(NSString*)url WithDelegate:(id)delegate onResponseCallback:(SEL)responseCallback
{
    [self GETRequestToURL:url WithDelegate:delegate onSuccessCallback:responseCallback onFailureCallback:responseCallback];
}

+ (void)GETRequestToURL:(NSString*)url WithDelegate:(id)delegate onSuccessCallback:(SEL)successCallback onFailureCallback:(SEL)failureCallback
{
    dispatch_async(dispatch_get_main_queue(), ^
                   {
                       NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:url]cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:60.0];
                       
                       [request setValue:@"gzip" forHTTPHeaderField:@"Accept-Encoding"];
                       
                       [request setHTTPMethod:@"GET"];
                       
                       NSUserDefaults * user=[[NSUserDefaults alloc]init];
                       StaticKeys * StaticKeysObject=[[StaticKeys alloc]init];
                       NSString * auth_key=[[NSString alloc]initWithFormat:@"Bearer %@",[user objectForKey:StaticKeysObject.auth_token]];

                       NSDictionary *headers = @{ @"accept": @"text/html",
                                                  @"content-type": @"application/json",@"Accept-Encoding":@"gzip",@"Authorization":auth_key};

                       [request setAllHTTPHeaderFields:headers];
                       request.cachePolicy=NSURLRequestReloadIgnoringCacheData;
                       NSURLSession * session = [NSURLSession sharedSession];
                       
                       NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:request
                                                                   completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                                                                       if (error)
                                                                       {
                                                                           dispatch_async(dispatch_get_main_queue(), ^
                                                                                          {
                                                                                              NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *)dataTask.response;
                                                                                              NSUInteger statusCode = httpResponse.statusCode;
                                                                                              
                                                                                              NSUserDefaults * user=[[NSUserDefaults alloc]init];
                                                                                              
                                                                                              NSString * makeMessage=[[NSString alloc]init];
                                                                                              
                                                                                              NSString * statusCodeToAdd=[[NSString alloc]initWithFormat:@" (%lu)",(unsigned long)statusCode];
                                                                                              
                                                                                              makeMessage=[makeMessage stringByAppendingString:statusCodeToAdd];
                                                                                              
                                                                                              switch (statusCode)
                                                                                              {
                                                                                                  case 504:
                                                                                                  {
                                                                                                      makeMessage=[makeMessage stringByAppendingString:@"Timeout error"];
                                                                                                      
                                                                                                      break;
                                                                                                  }
                                                                                                  case 503:
                                                                                                  {
                                                                                                      makeMessage=[makeMessage stringByAppendingString:@"Request incomplete,server overloaded"];
                                                                                                      break;
                                                                                                  }
                                                                                                  case 500:
                                                                                                  {
                                                                                                      makeMessage=[makeMessage stringByAppendingString:@"Request incomplete, close your app and try again"];
                                                                                                      break;
                                                                                                  }
                                                                                                  case 413:
                                                                                                  {
                                                                                                      makeMessage=[makeMessage stringByAppendingString:@"Too much of data, server will not accept"];
                                                                                                      break;
                                                                                                  }
                                                                                                  case 415:
                                                                                                  {
                                                                                                      makeMessage=[makeMessage stringByAppendingString:@"Media type not supported"];
                                                                                                      break;
                                                                                                  }
                                                                                                  case 408:
                                                                                                  {
                                                                                                      makeMessage=[makeMessage stringByAppendingString:@"Request took longer time, try again"];
                                                                                                      break;
                                                                                                  }
                                                                                                  case 401:
                                                                                                  {
                                                                                                      makeMessage=[makeMessage stringByAppendingString:@"You are not authorised, contact admin"];
                                                                                                      break;
                                                                                                  }
                                                                                                  case 403:
                                                                                                  {
                                                                                                      makeMessage=[makeMessage stringByAppendingString:@"Access Hidden, contact support team"];
                                                                                                      break;
                                                                                                  }
                                                                                                  case 405:
                                                                                                  {
                                                                                                      makeMessage=[makeMessage stringByAppendingString:@"Access not allowed, contact technical team"];
                                                                                                      
                                                                                                      break;
                                                                                                  }
                                                                                                  case 406:
                                                                                                  {
                                                                                                      makeMessage=[makeMessage stringByAppendingString:@"Request not acceptable, close the app and try again"];
                                                                                                      
                                                                                                      break;
                                                                                                  }
                                                                                                  case 400:
                                                                                                  {
                                                                                                      makeMessage=[makeMessage stringByAppendingString:@"Bad request, try later"];
                                                                                                      break;
                                                                                                  }
                                                                                                  case 302:
                                                                                                  {
                                                                                                      makeMessage=[makeMessage stringByAppendingString:@"Page removed, contact technical team"];
                                                                                                      break;
                                                                                                  }
                                                                                                  case 505:
                                                                                                  {
                                                                                                      
                                                                                                      makeMessage=[makeMessage stringByAppendingString:@"Server is blocking due to secuirity thread"];
                                                                                                      
                                                                                                      break;
                                                                                                  }
                                                                                                  default:
                                                                                                  {
                                                                                                      makeMessage=[makeMessage stringByAppendingString:error.description];
                                                                                                      break;
                                                                                                  }
                                                                                              }
                                                                                              
                                                                                              [user setObject:makeMessage forKey:@"ErrorResponseForShowing"];
                                                                                              NSLog(@"%@", error);
                                                                                              
                                                                                              [delegate performSelector:failureCallback withObject:error];
                                                                                          });
                                                                           
                                                                       }
                                                                       else
                                                                       {
                                                                           dispatch_async(dispatch_get_main_queue(), ^
                                                                                          {
                                                                                              NSError *error1;
                                                                                              NSMutableDictionary * innerJson = [NSJSONSerialization
                                                                                                                                 JSONObjectWithData:data options:kNilOptions error:&error1
                                                                                                                                 ];
                                                                                              [delegate performSelector:successCallback withObject:innerJson];
                                                                                          });
                                                                       }
                                                                   }];
                       [dataTask resume];
                   }
                   );
}

/*
 * POST request
 */
+(void)POSTRequestToURL:(NSString *)url withDelegate:(id)delegate withInfo:(NSDictionary *)params onResponseCallback:(SEL)responseCallback
{
    [self POSTRequestToURL:url withDelegate:delegate withInfo:params onSuccessCallback:responseCallback onFailureCallback:responseCallback];
}

+(void)POSTRequestToURL:(NSString *)url withDelegate:(id)delegate withInfo:(NSDictionary *)params onSuccessCallback:(SEL)successCallback onFailureCallback:(SEL)failureCallback
{
    dispatch_async(dispatch_get_main_queue(), ^
                   {
                       NSUserDefaults * user=[[NSUserDefaults alloc]init];
                       
                       StaticKeys * StaticKeysObject=[[StaticKeys alloc]init];
                       NSString * auth_key=[[NSString alloc]initWithFormat:@"Bearer %@",[user objectForKey:StaticKeysObject.auth_token]];
                       
                       NSDictionary *headers = @{ @"accept": @"text/html",
                                                  @"content-type": @"application/json",@"Accept-Encoding":@"gzip",@"Authorization":auth_key};
                       
                       NSData *postData = [NSJSONSerialization dataWithJSONObject:params options:0 error:nil];
                       
                       NSMutableURLRequest * request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:url]cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:60.0];
                       [request setHTTPMethod:@"POST"];
                       [request setHTTPBody:postData];
                       [request setAllHTTPHeaderFields:headers];
                       request.cachePolicy=NSURLRequestReloadIgnoringCacheData;
                       //    //[manager setValue:@"text/html" forKey:@"acceptableContentTypes"];
                       NSURLSession *session = [NSURLSession sharedSession];
                       NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:request
                                                                   completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                                                                       if (error)
                                                                       {
                                                                           dispatch_async(dispatch_get_main_queue(), ^
                                                                                          {
                                                                                              NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *)dataTask.response;
                                                                                              NSUInteger statusCode = httpResponse.statusCode;
                                                                                              
                                                                                              NSUserDefaults * user=[[NSUserDefaults alloc]init];
                                                                                              
                                                                                              NSString * makeMessage=[[NSString alloc]init];
                                                                                              
                                                                                              NSString * statusCodeToAdd=[[NSString alloc]initWithFormat:@" (%lu)",(unsigned long)statusCode];
                                                                                              
                                                                                              makeMessage=[makeMessage stringByAppendingString:statusCodeToAdd];
                                                                                              
                                                                                              switch (statusCode)
                                                                                              {
                                                                                                  case 504:
                                                                                                  {
                                                                                                      makeMessage=[makeMessage stringByAppendingString:@"Timeout error"];
                                                                                                      
                                                                                                      break;
                                                                                                  }
                                                                                                  case 503:
                                                                                                  {
                                                                                                      makeMessage=[makeMessage stringByAppendingString:@"Request incomplete,server overloaded"];
                                                                                                      break;
                                                                                                  }
                                                                                                  case 500:
                                                                                                  {
                                                                                                      makeMessage=[makeMessage stringByAppendingString:@"Request incomplete, close your app and try again"];
                                                                                                      break;
                                                                                                  }
                                                                                                  case 413:
                                                                                                  {
                                                                                                      makeMessage=[makeMessage stringByAppendingString:@"Too much of data, server will not accept"];
                                                                                                      break;
                                                                                                  }
                                                                                                  case 415:
                                                                                                  {
                                                                                                      makeMessage=[makeMessage stringByAppendingString:@"Media type not supported"];
                                                                                                      break;
                                                                                                  }
                                                                                                  case 408:
                                                                                                  {
                                                                                                      makeMessage=[makeMessage stringByAppendingString:@"Request took longer time, try again"];
                                                                                                      break;
                                                                                                  }
                                                                                                  case 401:
                                                                                                  {
                                                                                                      makeMessage=[makeMessage stringByAppendingString:@"You are not authorised, contact admin"];
                                                                                                      break;
                                                                                                  }
                                                                                                  case 403:
                                                                                                  {
                                                                                                      makeMessage=[makeMessage stringByAppendingString:@"Access Hidden, contact support team"];
                                                                                                      break;
                                                                                                  }
                                                                                                  case 405:
                                                                                                  {
                                                                                                      makeMessage=[makeMessage stringByAppendingString:@"Access not allowed, contact technical team"];
                                                                                                      
                                                                                                      break;
                                                                                                  }
                                                                                                  case 406:
                                                                                                  {
                                                                                                      makeMessage=[makeMessage stringByAppendingString:@"Request not acceptable, close the app and try again"];
                                                                                                      
                                                                                                      break;
                                                                                                  }
                                                                                                  case 400:
                                                                                                  {
                                                                                                      makeMessage=[makeMessage stringByAppendingString:@"Bad request, try later"];
                                                                                                      break;
                                                                                                  }
                                                                                                  case 302:
                                                                                                  {
                                                                                                      makeMessage=[makeMessage stringByAppendingString:@"Page removed, contact technical team"];
                                                                                                      break;
                                                                                                  }
                                                                                                  case 505:
                                                                                                  {
                                                                                                      
                                                                                                      makeMessage=[makeMessage stringByAppendingString:@"Server is blocking due to secuirity thread"];
                                                                                                      
                                                                                                      break;
                                                                                                  }
                                                                                                  default:
                                                                                                  {
                                                                                                      makeMessage=[makeMessage stringByAppendingString:error.description];
                                                                                                      break;
                                                                                                  }
                                                                                              }
                                                                                              
                                                                                              [user setObject:makeMessage forKey:@"ErrorResponseForShowing"];
                                                                                              NSLog(@"%@", error);
                                                                                              
                                                                                              [delegate performSelector:failureCallback withObject:error];
                                                                                          });
                                                                           
                                                                       }
                                                                       else
                                                                       {
                                                                           dispatch_async(dispatch_get_main_queue(), ^
                                                                                          {
                                                                                              NSError *error1;
                                                                                              NSMutableDictionary * innerJson = [NSJSONSerialization
                                                                                                                                 JSONObjectWithData:data options:kNilOptions error:&error1                                                                                   ];
                                                                                              [delegate performSelector:successCallback withObject:innerJson];
                                                                                          });
                                                                       }
                                                                   }];
                       [dataTask resume];
                   });
    
    //    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    //
    ////    AFSecurityPolicy * securityPolicy=[AFSecurityPolicy policyWithPinningMode:AFSSLPinningModeNone];
    ////    manager.securityPolicy=securityPolicy;
    //
    //    //[manager setValue:@"text/html" forKey:@"acceptableContentTypes"];
    //    [manager POST:url parameters:params progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject)
    //    {
    //        [delegate performSelector:successCallback withObject:responseObject];
    //    }
    //          failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error)
    //    {
    //        NSHTTPURLResponse *response = (NSHTTPURLResponse *)task.response;
    //        NSUInteger statusCode= (long)response.statusCode;
    //
    //        NSUserDefaults * user=[[NSUserDefaults alloc]init];
    //
    //        NSString * makeMessage=[[NSString alloc]init];
    //
    //        switch (statusCode)
    //        {
    //            case 504:
    //            {
    //                makeMessage=[[NSString alloc]initWithFormat:@"%@",@"Timeout error"];
    //                break;
    //            }
    //            case 503:
    //            {
    //                makeMessage=[[NSString alloc]initWithFormat:@"%@",@"Request incomplete,server overloaded"];
    //                break;
    //            }
    //            case 500:
    //            {
    //                makeMessage=[[NSString alloc]initWithFormat:@"%@",@"Request incomplete, close your app and try again"];
    //                break;
    //            }
    //            case 413:
    //            {
    //                makeMessage=[[NSString alloc]initWithFormat:@"%@",@"Too much of data, server will not accept"];
    //                break;
    //            }
    //            case 415:
    //            {
    //                makeMessage=[[NSString alloc]initWithFormat:@"%@",@"Media type not supported"];
    //                break;
    //            }
    //            case 408:
    //            {
    //                makeMessage=[[NSString alloc]initWithFormat:@"%@",@"Request took longer time, try again"];
    //                break;
    //            }
    //            case 401:
    //            {
    //                makeMessage=[[NSString alloc]initWithFormat:@"%@",@"You are not authorised, contact admin"];
    //                break;
    //            }
    //            case 403:
    //            {
    //                makeMessage=[[NSString alloc]initWithFormat:@"%@",@"Access Hidden, contact support team"];
    //                break;
    //            }
    //            case 405:
    //            {
    //                makeMessage=[[NSString alloc]initWithFormat:@"%@",@"Access not allowed, contact technical team"];
    //                break;
    //            }
    //            case 406:
    //            {
    //                makeMessage=[[NSString alloc]initWithFormat:@"%@",@"Request not acceptable, close the app and try again"];
    //                break;
    //            }
    //            case 400:
    //            {
    //                makeMessage=[[NSString alloc]initWithFormat:@"%@",@"Bad request, try later"];
    //                break;
    //            }
    //            case 302:
    //            {
    //                makeMessage=[[NSString alloc]initWithFormat:@"%@",@"Page removed, contact technical team"];
    //                break;
    //            }
    //            case 505:
    //            {
    //                makeMessage=[[NSString alloc]initWithFormat:@"%@",@"Server is blocking due to secuirity thread"];
    //                break;
    //            }
    //            default:
    //            {
    //                makeMessage=[[NSString alloc]initWithFormat:@"%@",error.description];
    //                break;
    //            }
    //        }
    //
    //        NSString * statusCodeToAdd=[[NSString alloc]initWithFormat:@" (%lu)",(unsigned long)statusCode];
    //
    //        makeMessage=[makeMessage stringByAppendingString:statusCodeToAdd];
    //
    //        [user setObject:makeMessage forKey:@"ErrorResponseForShowing"];
    //
    //        [delegate performSelector:failureCallback withObject:error];
    //    }];
}

+(void)POSTRequestWithHeaderImageTypeMultiPartDataToURLSingleImage:(NSString *)requestString :(NSData *)imageData :(NSMutableArray *)ParamsArray withDelegate:(id)delegate onSuccessCallback:(SEL)successCallback onFailureCallback:(SEL)failureCallback
{
    NSMutableURLRequest *request =[[NSMutableURLRequest alloc] init];
    [request setURL:[NSURL URLWithString:requestString]];
    [request setHTTPMethod:@"POST"];
    
    NSString *boundary = @"14737809831466499882746641444";
    NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@",boundary];
    [request addValue:contentType forHTTPHeaderField: @"Content-Type"];
    request.cachePolicy=NSURLRequestReloadIgnoringCacheData;
    //-- Append data into posr url using following method
    NSMutableData *body = [NSMutableData data];
    
    for (int k=0;k<ParamsArray.count;k++)
    {
        NSMutableDictionary * oneDict=ParamsArray[k];
        
        NSString * nameToSet=[[NSString alloc]initWithFormat:@"%@",[oneDict objectForKey:@"valueName"]];
        NSString * ValueToSet=[[NSString alloc]initWithFormat:@"%@",[oneDict objectForKey:@"value"]];
        
        [body appendData:[[NSString stringWithFormat:@"\r\n--%@\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"%@\"\r\n\r\n",nameToSet] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[[NSString stringWithFormat:@"%@",ValueToSet] dataUsingEncoding:NSUTF8StringEncoding]];
    }
    
    [body appendData:[[NSString stringWithFormat:@"\r\n--%@\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    
    [body appendData:[[NSString stringWithFormat:@"Content-Disposition:form-data; name=\"image\"; filename=\"%@\"\r\n",@"hello.png"] dataUsingEncoding:NSUTF8StringEncoding]];
    
    [body appendData:[@"Content-Type: image/png\r\n\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[NSData dataWithData:imageData]];
    
    [body appendData:[[NSString stringWithFormat:@"\r\n--%@--\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    //-- For sending image into service if needed (send image as imagedata)
    
    //-- "image_name" is file name of the image (we can set custom name)
    
    //-- Sending data into server through URL
    [request setHTTPBody:body];
    
    //-- Getting response form server
    NSData *responseData = [NSURLConnection sendSynchronousRequest:request returningResponse:nil error:nil];
    
    //-- JSON Parsing with response data
    id result = [NSJSONSerialization JSONObjectWithData:responseData options:NSJSONReadingMutableContainers error:nil];
    
    NSError *errorJson=nil;
    NSDictionary * responseDict = [NSJSONSerialization JSONObjectWithData:responseData options:kNilOptions error:&errorJson];
    
    NSLog(@"responseDict=%@",responseDict);
    
    NSLog(@"%@",result);
    
    NSString* renameResponse = [[NSString alloc] initWithData:responseData encoding:NSASCIIStringEncoding];
    
    if (responseData==nil)
    {
        [delegate performSelector:failureCallback withObject:result];
    }
    else
    {
        
        [delegate performSelector:successCallback withObject:result];
    }
}

@end
