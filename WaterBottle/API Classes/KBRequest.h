//
//  KBRequest.h
//  WaterBottle
//
//  Created by Kashyap Bhatt on 22/06/19.
//  Copyright © 2019 Kashyap. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface KBRequest : NSObject

+ (void)GETRequestToURL:(NSString*)url WithDelegate:(id)delegate onResponseCallback:(SEL)responseCallback;
+ (void)GETRequestToURL:(NSString*)url WithDelegate:(id)delegate onSuccessCallback:(SEL)successCallback onFailureCallback:(SEL)failureCallback;

+(void)POSTRequestToURL:(NSString *)url withDelegate:(id)delegate withInfo:(NSDictionary *)params onResponseCallback:(SEL)responseCallback;
+(void)POSTRequestToURL:(NSString *)url withDelegate:(id)delegate withInfo:(NSDictionary *)params onSuccessCallback:(SEL)successCallback onFailureCallback:(SEL)failureCallback;


@end

NS_ASSUME_NONNULL_END
