//
//  KBAPIService.h
//  WaterBottle
//
//  Created by Kashyap Bhatt on 22/06/19.
//  Copyright © 2019 Kashyap. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "KBRequest.h"

NS_ASSUME_NONNULL_BEGIN

@interface KBAPIService : KBRequest

+(NSString *) GetBaseURLForAPI;

// login with phone & password
+ (void)LoginWithPhoneNumber:(NSString *)phone Password:(NSString*)password withDelegate:(id)delegate onSuccessCallback:(SEL)successCallback onFailureCallback:(SEL)failureCallback;

// register user
+ (void)RegisterUser:(NSString *)name :(NSString *)email :(NSString *)country_code :(NSString *)phone :(NSString *)password withDelegate:(id)delegate onSuccessCallback:(SEL)successCallback onFailureCallback:(SEL)failureCallback;

// order list api for Admin

+ (void)OrderListAPI:(NSString *)delivery_user_id :(NSString *)from_date :(NSString *)to_date :(NSString *)order_status :(int)page_number :(NSString *)search_word withDelegate:(id)delegate onSuccessCallback:(SEL)successCallback onFailureCallback:(SEL)failureCallback;

// for getting all delivery executives
+ (void)GetDeliveryExecutives:(int)page_number :(NSString *)search_word withDelegate:(id)delegate onSuccessCallback:(SEL)successCallback onFailureCallback:(SEL)failureCallback;

// product list for customer
+ (void)GetCustomerProductList:(int)page_number :(NSString *)search_word withDelegate:(id)delegate onSuccessCallback:(SEL)successCallback onFailureCallback:(SEL)failureCallback;

// order list for customer
+ (void)GetCustomerOrderList:(NSString *)from_date :(NSString *)to_date :(NSString *)order_status :(int )page_number :(NSString *)search_word withDelegate:(id)delegate onSuccessCallback:(SEL)successCallback onFailureCallback:(SEL)failureCallback;

+ (void)AddCustomerProductToCart:(NSString *)product_id :(NSString *)quantity :(NSString *)return_quantity withDelegate:(id)delegate onSuccessCallback:(SEL)successCallback onFailureCallback:(SEL)failureCallback;

+ (void)GetCustomerCartDetailswithDelegate:(id)delegate onSuccessCallback:(SEL)successCallback onFailureCallback:(SEL)failureCallback;

+ (void)GetCustomerAddress:(int)page_number WithDelegate:(id)delegate onSuccessCallback:(SEL)successCallback onFailureCallback:(SEL)failureCallback;

+ (void)CustomerProceedToBuy:(NSString *)billing_address_id :(NSString *)payment_completion_status :(NSString *)payment_message WithDelegate:(id)delegate onSuccessCallback:(SEL)successCallback onFailureCallback:(SEL)failureCallback;

+ (void)LogoutUserWithDelegate:(id)delegate onSuccessCallback:(SEL)successCallback onFailureCallback:(SEL)failureCallback;

+ (void)AdminOrderApprove:(NSString *)order_id :(NSString *)approve_reject_status WithDelegate:(id)delegate onSuccessCallback:(SEL)successCallback onFailureCallback:(SEL)failureCallback;

// for getting the order list for delivery guy
+ (void)DeliveryOrderList:(NSString *)from_date :(NSString *)to_date :(int)page_number :(NSString *)search_word WithDelegate:(id)delegate onSuccessCallback:(SEL)successCallback onFailureCallback:(SEL)failureCallback;

// for getting notification list for delivery guy
+ (void)GetDeliveryNotificationList:(int)page_number WithDelegate:(id)delegate onSuccessCallback:(SEL)successCallback onFailureCallback:(SEL)failureCallback;

// for approving or declining the order for delivery guy
+ (void)DeliveryNotificationApproveReject:(NSString *)order_id :(NSString *)accept_reject_status :(NSString *)rejected_reason WithDelegate:(id)delegate onSuccessCallback:(SEL)successCallback onFailureCallback:(SEL)failureCallback;

// for deleting customer address
+ (void)DeleteCustomerAddress:(NSString *)address_id WithDelegate:(id)delegate onSuccessCallback:(SEL)successCallback onFailureCallback:(SEL)failureCallback;

// for assigning delivery person to order - ADMIN
+ (void)AssignDeliveryPerson:(NSString *)order_id :(NSString *)delivery_guy_id WithDelegate:(id)delegate onSuccessCallback:(SEL)successCallback onFailureCallback:(SEL)failureCallback;

// for getting order payment status for status filter
+ (void)GetOrderPaymentStatus:(NSString *)order_notification_payment WithDelegate:(id)delegate onSuccessCallback:(SEL)successCallback onFailureCallback:(SEL)failureCallback;
@end

NS_ASSUME_NONNULL_END
