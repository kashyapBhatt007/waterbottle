//
//  KBAPIService.m
//  WaterBottle
//
//  Created by Kashyap Bhatt on 22/06/19.
//  Copyright © 2019 Kashyap. All rights reserved.
//

#import "KBAPIService.h"
#import "StaticKeys.h"

@implementation KBAPIService


+(NSString *) GetStaticLoggedinId
{
    NSUserDefaults * user=[[NSUserDefaults alloc]init];
    
    StaticKeys * staticKeysObject=[[StaticKeys alloc]init];
    
    return [user objectForKey:staticKeysObject.user_id];
}

+(NSString *) GetBaseURLForAPI
{
    NSUserDefaults * user=[[NSUserDefaults alloc]init];
    NSString * baseUrl=[user objectForKey:@"BaseURL"];
    return baseUrl;
}

+ (void)LoginWithPhoneNumber:(NSString *)phone Password:(NSString*)password withDelegate:(id)delegate onSuccessCallback:(SEL)successCallback onFailureCallback:(SEL)failureCallback
{
    NSMutableDictionary * params=[[NSMutableDictionary alloc]init];
    [params setObject:phone forKey:@"phone"];
    [params setObject:password forKey:@"password"];
    
    NSString * baseURL=[self GetBaseURLForAPI];
    NSString * ApiUrlString=[[NSString alloc]initWithFormat:@"%@/api/login",baseURL];
    
    [KBRequest POSTRequestToURL:ApiUrlString withDelegate:delegate withInfo:params onSuccessCallback:successCallback onFailureCallback:failureCallback];
}

+ (void)RegisterUser:(NSString *)name :(NSString *)email :(NSString *)country_code :(NSString *)phone :(NSString *)password withDelegate:(id)delegate onSuccessCallback:(SEL)successCallback onFailureCallback:(SEL)failureCallback
{
    NSMutableDictionary * params=[[NSMutableDictionary alloc]init];
    [params setObject:name forKey:@"name"];
    [params setObject:email forKey:@"email"];
    [params setObject:country_code forKey:@"country_code"];
    [params setObject:phone forKey:@"phone"];
    [params setObject:password forKey:@"password"];
    
    NSString * baseURL=[self GetBaseURLForAPI];
    NSString * ApiUrlString=[[NSString alloc]initWithFormat:@"%@/api/customer-registration",baseURL];
    
    [KBRequest POSTRequestToURL:ApiUrlString withDelegate:delegate withInfo:params onSuccessCallback:successCallback onFailureCallback:failureCallback];
}

+ (void)OrderListAPI:(NSString *)delivery_user_id :(NSString *)from_date :(NSString *)to_date :(NSString *)order_status :(int)page_number :(NSString *)search_word withDelegate:(id)delegate onSuccessCallback:(SEL)successCallback onFailureCallback:(SEL)failureCallback
{
    NSString * timezone_app_offset=[[NSString alloc]initWithFormat:@"%ld",(long)[[NSTimeZone localTimeZone] secondsFromGMT]];
    
    NSMutableDictionary * params=[[NSMutableDictionary alloc]init];
    [params setObject:timezone_app_offset forKey:@"timezone_app_offset"];

    NSString * pageToSend=[[NSString alloc]initWithFormat:@"%d",page_number];
    
    [params setObject:[self GetStaticLoggedinId] forKey:@"logged_in_id"];
    [params setObject:delivery_user_id forKey:@"delivery_user_id"];
    [params setObject:from_date forKey:@"from_date"];
    [params setObject:to_date forKey:@"to_date"];
    [params setObject:order_status forKey:@"order_status"];
    [params setObject:pageToSend forKey:@"page_number"];
    [params setObject:search_word forKey:@"search_word"];
    
    NSString * baseURL=[self GetBaseURLForAPI];
    NSString * ApiUrlString=[[NSString alloc]initWithFormat:@"%@/api/admin-order-page",baseURL];
    
    [KBRequest POSTRequestToURL:ApiUrlString withDelegate:delegate withInfo:params onSuccessCallback:successCallback onFailureCallback:failureCallback];
}

+ (void)GetDeliveryExecutives:(int)page_number :(NSString *)search_word withDelegate:(id)delegate onSuccessCallback:(SEL)successCallback onFailureCallback:(SEL)failureCallback
{
    NSString * timezone_app_offset=[[NSString alloc]initWithFormat:@"%ld",(long)[[NSTimeZone localTimeZone] secondsFromGMT]];
    
    NSMutableDictionary * params=[[NSMutableDictionary alloc]init];
    [params setObject:timezone_app_offset forKey:@"timezone_app_offset"];
    
    NSString * pageToSend=[[NSString alloc]initWithFormat:@"%d",page_number];
    
    [params setObject:[self GetStaticLoggedinId] forKey:@"logged_in_id"];
    
    [params setObject:pageToSend forKey:@"page_number"];
    [params setObject:search_word forKey:@"search_word"];
    
    NSString * baseURL=[self GetBaseURLForAPI];
    NSString * ApiUrlString=[[NSString alloc]initWithFormat:@"%@/api/fetch-delivery-users-for-filter",baseURL];
    
    [KBRequest POSTRequestToURL:ApiUrlString withDelegate:delegate withInfo:params onSuccessCallback:successCallback onFailureCallback:failureCallback];
}

+ (void)GetCustomerProductList:(int)page_number :(NSString *)search_word withDelegate:(id)delegate onSuccessCallback:(SEL)successCallback onFailureCallback:(SEL)failureCallback
{
    NSString * timezone_app_offset=[[NSString alloc]initWithFormat:@"%ld",(long)[[NSTimeZone localTimeZone] secondsFromGMT]];
    
    NSMutableDictionary * params=[[NSMutableDictionary alloc]init];
    [params setObject:timezone_app_offset forKey:@"timezone_app_offset"];
    
    NSString * pageToSend=[[NSString alloc]initWithFormat:@"%d",page_number];
    
    [params setObject:[self GetStaticLoggedinId] forKey:@"customer_id"];
    
    [params setObject:pageToSend forKey:@"page_number"];
    [params setObject:search_word forKey:@"search_word"];
    
    NSString * baseURL=[self GetBaseURLForAPI];
    NSString * ApiUrlString=[[NSString alloc]initWithFormat:@"%@/api/customer-product-list-page",baseURL];
    
    [KBRequest POSTRequestToURL:ApiUrlString withDelegate:delegate withInfo:params onSuccessCallback:successCallback onFailureCallback:failureCallback];
}

+ (void)GetCustomerOrderList:(NSString *)from_date :(NSString *)to_date :(NSString *)order_status :(int )page_number :(NSString *)search_word withDelegate:(id)delegate onSuccessCallback:(SEL)successCallback onFailureCallback:(SEL)failureCallback
{
    NSString * timezone_app_offset=[[NSString alloc]initWithFormat:@"%ld",(long)[[NSTimeZone localTimeZone] secondsFromGMT]];
    
    NSMutableDictionary * params=[[NSMutableDictionary alloc]init];
    [params setObject:timezone_app_offset forKey:@"timezone_app_offset"];
    
    NSString * pageToSend=[[NSString alloc]initWithFormat:@"%d",page_number];
    
    [params setObject:[self GetStaticLoggedinId] forKey:@"customer_id"];
    
    [params setObject:pageToSend forKey:@"page_number"];
    [params setObject:search_word forKey:@"search_word"];
    [params setObject:from_date forKey:@"from_date"];
    [params setObject:to_date forKey:@"to_date"];
    [params setObject:order_status forKey:@"order_status"];

    NSString * baseURL=[self GetBaseURLForAPI];
    NSString * ApiUrlString=[[NSString alloc]initWithFormat:@"%@/api/customer-order-page",baseURL];
    
    [KBRequest POSTRequestToURL:ApiUrlString withDelegate:delegate withInfo:params onSuccessCallback:successCallback onFailureCallback:failureCallback];
}

+ (void)AddCustomerProductToCart:(NSString *)product_id :(NSString *)quantity :(NSString *)return_quantity withDelegate:(id)delegate onSuccessCallback:(SEL)successCallback onFailureCallback:(SEL)failureCallback
{
    NSString * timezone_app_offset=[[NSString alloc]initWithFormat:@"%ld",(long)[[NSTimeZone localTimeZone] secondsFromGMT]];
    
    NSMutableDictionary * params=[[NSMutableDictionary alloc]init];
    [params setObject:timezone_app_offset forKey:@"timezone_app_offset"];
    
    [params setObject:[self GetStaticLoggedinId] forKey:@"customer_id"];
    
    [params setObject:product_id forKey:@"product_id"];
    [params setObject:quantity forKey:@"quantity"];
    [params setObject:return_quantity forKey:@"return_quantity"];
    
    NSString * baseURL=[self GetBaseURLForAPI];
    NSString * ApiUrlString=[[NSString alloc]initWithFormat:@"%@/api/add-product-into-cart",baseURL];
    
    [KBRequest POSTRequestToURL:ApiUrlString withDelegate:delegate withInfo:params onSuccessCallback:successCallback onFailureCallback:failureCallback];
}

+ (void)GetCustomerCartDetailswithDelegate:(id)delegate onSuccessCallback:(SEL)successCallback onFailureCallback:(SEL)failureCallback
{
    NSString * timezone_app_offset=[[NSString alloc]initWithFormat:@"%ld",(long)[[NSTimeZone localTimeZone] secondsFromGMT]];
    
    NSMutableDictionary * params=[[NSMutableDictionary alloc]init];
    [params setObject:timezone_app_offset forKey:@"timezone_app_offset"];
    
    [params setObject:[self GetStaticLoggedinId] forKey:@"customer_id"];
    
    NSString * baseURL=[self GetBaseURLForAPI];
    NSString * ApiUrlString=[[NSString alloc]initWithFormat:@"%@/api/customer-products-in-cart",baseURL];
    
    [KBRequest POSTRequestToURL:ApiUrlString withDelegate:delegate withInfo:params onSuccessCallback:successCallback onFailureCallback:failureCallback];
}

+ (void)GetCustomerAddress:(int)page_number WithDelegate:(id)delegate onSuccessCallback:(SEL)successCallback onFailureCallback:(SEL)failureCallback
{
    NSString * timezone_app_offset=[[NSString alloc]initWithFormat:@"%ld",(long)[[NSTimeZone localTimeZone] secondsFromGMT]];
    
    NSMutableDictionary * params=[[NSMutableDictionary alloc]init];
    [params setObject:timezone_app_offset forKey:@"timezone_app_offset"];
    
    [params setObject:[self GetStaticLoggedinId] forKey:@"customer_id"];
    
    NSString * pageToSend=[[NSString alloc]initWithFormat:@"%d",page_number];
    
    [params setObject:pageToSend forKey:@"page_number"];
    
    NSString * baseURL=[self GetBaseURLForAPI];
    NSString * ApiUrlString=[[NSString alloc]initWithFormat:@"%@/api/customer-addresses-list",baseURL];
    
    [KBRequest POSTRequestToURL:ApiUrlString withDelegate:delegate withInfo:params onSuccessCallback:successCallback onFailureCallback:failureCallback];
}

+ (void)CustomerProceedToBuy:(NSString *)billing_address_id :(NSString *)payment_completion_status :(NSString *)payment_message WithDelegate:(id)delegate onSuccessCallback:(SEL)successCallback onFailureCallback:(SEL)failureCallback
{
    NSString * timezone_app_offset=[[NSString alloc]initWithFormat:@"%ld",(long)[[NSTimeZone localTimeZone] secondsFromGMT]];
    
    NSMutableDictionary * params=[[NSMutableDictionary alloc]init];
    [params setObject:timezone_app_offset forKey:@"timezone_app_offset"];
    
    [params setObject:[self GetStaticLoggedinId] forKey:@"customer_id"];
    
    [params setObject:billing_address_id forKey:@"billing_address_id"];
    [params setObject:payment_completion_status forKey:@"payment_completion_status"];
    [params setObject:@"" forKey:@"payment_message"];
    
    NSString * baseURL=[self GetBaseURLForAPI];
    NSString * ApiUrlString=[[NSString alloc]initWithFormat:@"%@/api/add-customer-order",baseURL];
    
    [KBRequest POSTRequestToURL:ApiUrlString withDelegate:delegate withInfo:params onSuccessCallback:successCallback onFailureCallback:failureCallback];
}

+ (void)LogoutUserWithDelegate:(id)delegate onSuccessCallback:(SEL)successCallback onFailureCallback:(SEL)failureCallback
{
    NSString * baseURL=[self GetBaseURLForAPI];
    NSString * ApiUrlString=[[NSString alloc]initWithFormat:@"%@/api/logout",baseURL];
    
    [KBRequest GETRequestToURL:ApiUrlString WithDelegate:delegate onSuccessCallback:successCallback onFailureCallback:failureCallback];
}

+ (void)AdminOrderApprove:(NSString *)order_id :(NSString *)approve_reject_status WithDelegate:(id)delegate onSuccessCallback:(SEL)successCallback onFailureCallback:(SEL)failureCallback
{
    NSString * timezone_app_offset=[[NSString alloc]initWithFormat:@"%ld",(long)[[NSTimeZone localTimeZone] secondsFromGMT]];
    
    NSMutableDictionary * params=[[NSMutableDictionary alloc]init];
    [params setObject:timezone_app_offset forKey:@"timezone_app_offset"];
    
    [params setObject:[self GetStaticLoggedinId] forKey:@"logged_in_id"];
    
    [params setObject:order_id forKey:@"order_id"];
    [params setObject:approve_reject_status forKey:@"approve_reject_status"];

    
    NSString * baseURL=[self GetBaseURLForAPI];
    NSString * ApiUrlString=[[NSString alloc]initWithFormat:@"%@/api/admin-approve-reject-order",baseURL];
    
    [KBRequest POSTRequestToURL:ApiUrlString withDelegate:delegate withInfo:params onSuccessCallback:successCallback onFailureCallback:failureCallback];
}

+ (void)DeliveryOrderList:(NSString *)from_date :(NSString *)to_date :(int)page_number :(NSString *)search_word WithDelegate:(id)delegate onSuccessCallback:(SEL)successCallback onFailureCallback:(SEL)failureCallback
{
    NSString * timezone_app_offset=[[NSString alloc]initWithFormat:@"%ld",(long)[[NSTimeZone localTimeZone] secondsFromGMT]];
    
    NSMutableDictionary * params=[[NSMutableDictionary alloc]init];
    [params setObject:timezone_app_offset forKey:@"timezone_app_offset"];
    
    [params setObject:[self GetStaticLoggedinId] forKey:@"delivery_guy_id"];
    
    NSString * pageToSend=[[NSString alloc]initWithFormat:@"%d",page_number];

    [params setObject:from_date forKey:@"from_date"];
    [params setObject:to_date forKey:@"to_date"];
    [params setObject:pageToSend forKey:@"page_number"];
    [params setObject:search_word forKey:@"search_word"];
    
    NSString * baseURL=[self GetBaseURLForAPI];
    NSString * ApiUrlString=[[NSString alloc]initWithFormat:@"%@/api/delivery-order-page",baseURL];
    
    [KBRequest POSTRequestToURL:ApiUrlString withDelegate:delegate withInfo:params onSuccessCallback:successCallback onFailureCallback:failureCallback];
}

+ (void)GetDeliveryNotificationList:(int)page_number WithDelegate:(id)delegate onSuccessCallback:(SEL)successCallback onFailureCallback:(SEL)failureCallback
{
    NSString * timezone_app_offset=[[NSString alloc]initWithFormat:@"%ld",(long)[[NSTimeZone localTimeZone] secondsFromGMT]];
    
    NSMutableDictionary * params=[[NSMutableDictionary alloc]init];
    [params setObject:timezone_app_offset forKey:@"timezone_app_offset"];
    
    [params setObject:[self GetStaticLoggedinId] forKey:@"delivery_guy_id"];
    
    NSString * pageToSend=[[NSString alloc]initWithFormat:@"%d",page_number];
    
    [params setObject:pageToSend forKey:@"page_number"];
    
    NSString * baseURL=[self GetBaseURLForAPI];
    NSString * ApiUrlString=[[NSString alloc]initWithFormat:@"%@/api/delivery-order-notification-page",baseURL];
    
    [KBRequest POSTRequestToURL:ApiUrlString withDelegate:delegate withInfo:params onSuccessCallback:successCallback onFailureCallback:failureCallback];
}

+ (void)DeliveryNotificationApproveReject:(NSString *)order_id :(NSString *)accept_reject_status :(NSString *)rejected_reason WithDelegate:(id)delegate onSuccessCallback:(SEL)successCallback onFailureCallback:(SEL)failureCallback
{
    NSString * timezone_app_offset=[[NSString alloc]initWithFormat:@"%ld",(long)[[NSTimeZone localTimeZone] secondsFromGMT]];
    
    NSMutableDictionary * params=[[NSMutableDictionary alloc]init];
    [params setObject:timezone_app_offset forKey:@"timezone_app_offset"];
    
    [params setObject:[self GetStaticLoggedinId] forKey:@"delivery_guy_id"];
    [params setObject:order_id forKey:@"order_id"];
    [params setObject:accept_reject_status forKey:@"accept_reject_status"];
    [params setObject:rejected_reason forKey:@"rejected_reason"];
    
    NSString * baseURL=[self GetBaseURLForAPI];
    NSString * ApiUrlString=[[NSString alloc]initWithFormat:@"%@/api/delivery-accept-reject-order",baseURL];
    
    [KBRequest POSTRequestToURL:ApiUrlString withDelegate:delegate withInfo:params onSuccessCallback:successCallback onFailureCallback:failureCallback];
}

+ (void)DeleteCustomerAddress:(NSString *)address_id WithDelegate:(id)delegate onSuccessCallback:(SEL)successCallback onFailureCallback:(SEL)failureCallback
{
    NSString * timezone_app_offset=[[NSString alloc]initWithFormat:@"%ld",(long)[[NSTimeZone localTimeZone] secondsFromGMT]];
    
    NSMutableDictionary * params=[[NSMutableDictionary alloc]init];
    [params setObject:timezone_app_offset forKey:@"timezone_app_offset"];
    
    [params setObject:address_id forKey:@"address_id"];
    
    NSString * baseURL=[self GetBaseURLForAPI];
    NSString * ApiUrlString=[[NSString alloc]initWithFormat:@"%@/api/delete-customer-address",baseURL];
    
    [KBRequest POSTRequestToURL:ApiUrlString withDelegate:delegate withInfo:params onSuccessCallback:successCallback onFailureCallback:failureCallback];
}

+ (void)AssignDeliveryPerson:(NSString *)order_id :(NSString *)delivery_guy_id WithDelegate:(id)delegate onSuccessCallback:(SEL)successCallback onFailureCallback:(SEL)failureCallback
{
    NSString * timezone_app_offset=[[NSString alloc]initWithFormat:@"%ld",(long)[[NSTimeZone localTimeZone] secondsFromGMT]];
    
    NSMutableDictionary * params=[[NSMutableDictionary alloc]init];
    [params setObject:timezone_app_offset forKey:@"timezone_app_offset"];
    [params setObject:[self GetStaticLoggedinId] forKey:@"logged_in_id"];
    [params setObject:order_id forKey:@"order_id"];
    [params setObject:delivery_guy_id forKey:@"delivery_guy_id"];
    
    NSString * baseURL=[self GetBaseURLForAPI];
    NSString * ApiUrlString=[[NSString alloc]initWithFormat:@"%@/api/update-order-delivery-guy",baseURL];
    
    [KBRequest POSTRequestToURL:ApiUrlString withDelegate:delegate withInfo:params onSuccessCallback:successCallback onFailureCallback:failureCallback];
}

+ (void)GetOrderPaymentStatus:(NSString *)order_notification_payment WithDelegate:(id)delegate onSuccessCallback:(SEL)successCallback onFailureCallback:(SEL)failureCallback
{
    NSMutableDictionary * params=[[NSMutableDictionary alloc]init];
    [params setObject:order_notification_payment forKey:@"order_notification_payment"];
    
    NSString * baseURL=[self GetBaseURLForAPI];
    NSString * ApiUrlString=[[NSString alloc]initWithFormat:@"%@/api/fetch-order-completion-status-for-fiter",baseURL];
    
    [KBRequest POSTRequestToURL:ApiUrlString withDelegate:delegate withInfo:params onSuccessCallback:successCallback onFailureCallback:failureCallback];
}
@end
