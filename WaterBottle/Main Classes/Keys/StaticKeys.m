//
//  StaticKeys.m
//  WaterBottle
//
//  Created by Kashyap Bhatt on 22/07/19.
//  Copyright © 2019 Kashyap. All rights reserved.
//

#import "StaticKeys.h"

@implementation StaticKeys

- (instancetype)init
{
    self = [super init];
    if (self) {
        self.auth_token=@"auth_token";
        self.country_code=@"country_code";
        self.email=@"email";
        self.user_id=@"user_id";
        self.name=@"name";
        self.phone=@"phone";
        self.user_role=@"user_role";
    }
    return self;
}
@end
