//
//  StaticKeys.h
//  WaterBottle
//
//  Created by Kashyap Bhatt on 22/07/19.
//  Copyright © 2019 Kashyap. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface StaticKeys : NSObject

@property NSString * auth_token;
@property NSString * country_code;
@property NSString * email;
@property NSString * user_id;
@property NSString * name;
@property NSString * phone;
@property NSString * user_role;

@end

NS_ASSUME_NONNULL_END
