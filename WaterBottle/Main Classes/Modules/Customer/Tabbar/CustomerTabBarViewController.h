//
//  CustomerTabBarViewController.h
//  WaterBottle
//
//  Created by Kashyap Bhatt on 29/07/19.
//  Copyright © 2019 Kashyap. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface CustomerTabBarViewController : UITabBarController

@end

NS_ASSUME_NONNULL_END
