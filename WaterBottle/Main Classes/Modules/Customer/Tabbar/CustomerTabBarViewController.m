//
//  CustomerTabBarViewController.m
//  WaterBottle
//
//  Created by Kashyap Bhatt on 29/07/19.
//  Copyright © 2019 Kashyap. All rights reserved.
//

#import "CustomerTabBarViewController.h"
#import "CustomerOrdersViewController.h"
#import "CustomerHomeViewController.h"
#import "CustomerCartViewController.h"
#import "CustomerProfileViewController.h"

@interface CustomerTabBarViewController ()

@end

@implementation CustomerTabBarViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    CustomerHomeViewController * view=[[CustomerHomeViewController alloc]init];
    CustomerOrdersViewController * view1=[[CustomerOrdersViewController alloc]init];
    CustomerCartViewController * view2=[[CustomerCartViewController alloc]init];
    CustomerProfileViewController * view3=[[CustomerProfileViewController alloc]init];
    
    NSMutableArray *tabViewControllers = [[NSMutableArray alloc] init];
    [tabViewControllers addObject:view];
    [tabViewControllers addObject:view1];
    [tabViewControllers addObject:view2];
    [tabViewControllers addObject:view3];
    
    [self setViewControllers:tabViewControllers];
    
    self.tabBarItem.imageInsets = UIEdgeInsetsMake(16, 10, -16, 10);
    
    [[UITabBar appearance] setBarTintColor:[UIColor whiteColor]];
    
    // setting up labels and icons for tab bar
    
    // for home
    UIImage * MyProjectSelected=[UIImage imageNamed:@"tab_home_selected"];
    view.tabBarItem.selectedImage = [MyProjectSelected
                                      imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    [view.tabBarItem setImageInsets:UIEdgeInsetsMake(5, 0, -5, 0)];
    view.tabBarItem.image = [[UIImage imageNamed:@"tab_home_unselected"]
                              imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    view.tabBarItem.title=@"Home";
    view.tabBarItem.tag=0;
    
    // for orders
    UIImage * MyProjectSelected1=[UIImage imageNamed:@"tab_order_selected"];
    view1.tabBarItem.selectedImage = [MyProjectSelected1
                                     imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    [view1.tabBarItem setImageInsets:UIEdgeInsetsMake(5, 0, -5, 0)];
    view1.tabBarItem.image = [[UIImage imageNamed:@"tab_order_unselected"]
                             imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    view1.tabBarItem.title=@"Orders";
    view1.tabBarItem.tag=1;
    
    // for cart
    UIImage * MyProjectSelected2=[UIImage imageNamed:@"tab_cart_selected"];
    view2.tabBarItem.selectedImage = [MyProjectSelected2
                                      imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    [view2.tabBarItem setImageInsets:UIEdgeInsetsMake(5, 0, -5, 0)];
    view2.tabBarItem.image = [[UIImage imageNamed:@"tab_cart_unselected"]
                              imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    view2.tabBarItem.title=@"Cart";
    view2.tabBarItem.tag=2;
    
    // for profile
    UIImage * MyProjectSelected3=[UIImage imageNamed:@"tab_profile_selected"];
    view3.tabBarItem.selectedImage = [MyProjectSelected3
                                      imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    [view3.tabBarItem setImageInsets:UIEdgeInsetsMake(5, 0, -5, 0)];
    view3.tabBarItem.image = [[UIImage imageNamed:@"tab_profile_unselected"]
                              imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    view3.tabBarItem.title=@"Profile";
    view3.tabBarItem.tag=3;
    
    self.selectedIndex=0;
}

@end
