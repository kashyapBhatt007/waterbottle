//
//  TitleNameTableViewCell.h
//  WaterBottle
//
//  Created by Kashyap Bhatt on 03/08/19.
//  Copyright © 2019 Kashyap. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface TitleNameTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *labelTitle;

@end

NS_ASSUME_NONNULL_END
