//
//  CustomerAddressTableViewCell.h
//  WaterBottle
//
//  Created by Ironman on 03/08/19.
//  Copyright © 2019 Kashyap. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface CustomerAddressTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIView *backViewCategory;
@property (weak, nonatomic) IBOutlet UIImageView *imageCategory;
@property (weak, nonatomic) IBOutlet UILabel *labelCategory;
@property (weak, nonatomic) IBOutlet UILabel *labelAddress;
@property (weak, nonatomic) IBOutlet UIButton *buttonEdit;
@property (weak, nonatomic) IBOutlet UIButton *buttonDelete;

@end

NS_ASSUME_NONNULL_END
