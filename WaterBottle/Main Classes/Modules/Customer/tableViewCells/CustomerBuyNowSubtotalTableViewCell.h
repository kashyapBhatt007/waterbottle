//
//  CustomerBuyNowSubtotalTableViewCell.h
//  WaterBottle
//
//  Created by Kashyap Bhatt on 03/08/19.
//  Copyright © 2019 Kashyap. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface CustomerBuyNowSubtotalTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *labelChange;

@property (weak, nonatomic) IBOutlet UILabel * labelSubtotalValue;
@property (weak, nonatomic) IBOutlet UILabel * labelDeliveryValue;
@property (weak, nonatomic) IBOutlet UILabel * labelTotalCharge;
@property (weak, nonatomic) IBOutlet UILabel * labelAddress;
@property (weak, nonatomic) IBOutlet UIButton * buttonChangeAddress;
@property (weak, nonatomic) IBOutlet UIButton * buttonProceedToBuy;
@property (weak, nonatomic) IBOutlet UIView * backViewForProceedButton;

@end

NS_ASSUME_NONNULL_END
