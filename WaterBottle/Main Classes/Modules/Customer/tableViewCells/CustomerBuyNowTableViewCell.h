//
//  CustomerBuyNowTableViewCell.h
//  WaterBottle
//
//  Created by Kashyap Bhatt on 02/08/19.
//  Copyright © 2019 Kashyap. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface CustomerBuyNowTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIView *backViewForProductImage;

@property (weak, nonatomic) IBOutlet UIView *BackView;
@property (weak, nonatomic) IBOutlet UIImageView *imageProduct;
@property (weak, nonatomic) IBOutlet UILabel *labelProductName;
@property (weak, nonatomic) IBOutlet UILabel *labelMeasurements;
@property (weak, nonatomic) IBOutlet UILabel *labelPrice;

@property (weak, nonatomic) IBOutlet UILabel *labelQuantity;
@property (weak, nonatomic) IBOutlet UILabel *labelRetunQuantity;

@end

NS_ASSUME_NONNULL_END
