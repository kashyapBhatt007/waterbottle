//
//  CustomerOrderTableViewCell.h
//  WaterBottle
//
//  Created by Kashyap Bhatt on 29/07/19.
//  Copyright © 2019 Kashyap. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface CustomerOrderTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIView *backViewForProductImage;

@property (weak, nonatomic) IBOutlet UIView *BackView;
@property (weak, nonatomic) IBOutlet UIImageView *imageProduct;
@property (weak, nonatomic) IBOutlet UILabel *labelProductName;
@property (weak, nonatomic) IBOutlet UILabel *labelMeasurements;
@property (weak, nonatomic) IBOutlet UILabel *labelPrice;
@property (weak, nonatomic) IBOutlet UIView *backViewAddProduct;
@property (weak, nonatomic) IBOutlet UIImageView *imaegCheckbox;
@property (weak, nonatomic) IBOutlet UIButton *buttonAddToCart;
@property (weak, nonatomic) IBOutlet UIButton *buttonCheckbox;
@property (weak, nonatomic) IBOutlet UIView *backViewForQuantity;
@property (weak, nonatomic) IBOutlet UIView *backViewForReturnQuantity;
@property (weak, nonatomic) IBOutlet UILabel *labelQuantity;
@property (weak, nonatomic) IBOutlet UIButton *buttonIncreaseQuantity;
@property (weak, nonatomic) IBOutlet UIButton *buttonDecreaseQuantity;
@property (weak, nonatomic) IBOutlet UIButton *buttonDecreaseReturnQuantity;
@property (weak, nonatomic) IBOutlet UIButton *buttonIncreaseReturnQuantity;
@property (weak, nonatomic) IBOutlet UILabel *labelRetunQuantity;
@property (weak, nonatomic) IBOutlet UIView *firstBackView;
@property (weak, nonatomic) IBOutlet UIView *secondBackView;

@end

NS_ASSUME_NONNULL_END
