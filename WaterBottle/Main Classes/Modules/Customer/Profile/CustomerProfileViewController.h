//
//  CustomerProfileViewController.h
//  WaterBottle
//
//  Created by Ironman on 04/08/19.
//  Copyright © 2019 Kashyap. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface CustomerProfileViewController : UIViewController
- (IBAction)Logout:(id)sender;

@end

NS_ASSUME_NONNULL_END
