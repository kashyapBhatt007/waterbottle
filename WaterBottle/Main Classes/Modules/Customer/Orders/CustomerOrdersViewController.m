//
//  CustomerOrdersViewController.m
//  WaterBottle
//
//  Created by Kashyap Bhatt on 29/07/19.
//  Copyright © 2019 Kashyap. All rights reserved.
//

#import "CustomerOrdersViewController.h"
#import "KBAPIService.h"
#import "KBSpinner.h"
#import "Internet.h"
#import "KBAlert.h"
#import "CustomerNewOrderTableViewCell.h"
#import <SDWebImage/UIImageView+WebCache.h>

@interface CustomerOrdersViewController ()

@end

@implementation CustomerOrdersViewController

-(void)viewDidAppear:(BOOL)animated
{
    [self InitializeValues];
    
    [self CallApi];
}
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    _labelEmpty.hidden=true;
}

-(void)InitializeValues
{
    _tableArray=[[NSMutableArray alloc]init];
    
    _from_date=[[NSString alloc]initWithFormat:@"%@",@""];
    _to_date=[[NSString alloc]initWithFormat:@"%@",@""];
    _order_status=[[NSString alloc]initWithFormat:@"%@",@"0"];
    _page_number=0;
    _search_word=[[NSString alloc]initWithFormat:@"%@",@""];
}

-(void)CallApi
{
    [KBSpinner show];
    
    [KBAPIService GetCustomerOrderList:_from_date :_to_date :_order_status :_page_number :_search_word withDelegate:self onSuccessCallback:@selector(GotList:) onFailureCallback:@selector(ErrorResponse:)];
}

-(void)GotList:(id)response
{
    NSLog(@"%@",response);
    NSString * status=[[NSString alloc]initWithFormat:@"%@",[response objectForKey:@"status"]];
    
    [KBSpinner dismiss];
    if ([status isEqualToString:@"1"])
    {
        NSMutableArray * tempArray=[response objectForKey:@"orderList"];
        
        if (_page_number==0)
        {
            NSString * totalCount=[[NSString alloc]initWithFormat:@"%@",[response objectForKey:@"totalCount"]];
            
            _TotalCount=[totalCount integerValue];
            
            if (tempArray.count==0)
            {
                _labelEmpty.hidden=false;
            }
            else
            {
                _labelEmpty.hidden=true;
            }
        }
        
        for (int k=0;k<tempArray.count;k++)
        {
            [_tableArray addObject:tempArray[k]];
        }
        
        [_table reloadData];
    }
    else
    {
        
    }
}

-(void)ErrorResponse:(id)response
{
    NSLog(@"%@",response);
}

-(void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
    NSString * InternetAvailability=[Internet CheckInternet];
    if ([InternetAvailability isEqualToString:@"Yes"])
    {
        if (_TotalCount/10>_page_number)
        {
            _page_number++;
            
            [self CallApi];
        }
    }
    else
    {
        [KBAlert ShowInternetToast];
    }
}

// tableview methods
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return _tableArray.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *simpleTableIdentifier = @"CustomerNewOrderTableViewCell";
    
    CustomerNewOrderTableViewCell *cell = (CustomerNewOrderTableViewCell *)[tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
    
    if (cell == nil)
    {
        NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"CustomerNewOrderTableViewCell" owner:self options:nil];
        cell = [nib objectAtIndex:0];
    }
    
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    [cell.BackView.layer setCornerRadius:10.0f];
    [cell.BackView.layer setShadowColor:[UIColor groupTableViewBackgroundColor].CGColor];
    [cell.BackView.layer setShadowOpacity:2.0];
    [cell.BackView.layer setShadowRadius:5.0];
    [cell.BackView.layer setShadowOffset:CGSizeMake(0.0f, 2.0f)];
    
    NSMutableDictionary * oneDict=[_tableArray objectAtIndex:indexPath.row];
    
    cell.LabelId.text=[[NSString alloc]initWithFormat:@"ID : %@",[oneDict objectForKey:@"order_number"]];
    
    cell.labelAssignedTo.text=[[NSString alloc]initWithFormat:@"%@",[oneDict objectForKey:@"billing_address"]];
    
    cell.labelAddress.text=[[NSString alloc]initWithFormat:@"%@",[oneDict objectForKey:@"total_price"]];
    
    NSURL * FinalURL=[NSURL URLWithString:[oneDict objectForKey:@"order_image_icon"]];
    
    [cell.ImageProduct sd_setImageWithURL:FinalURL placeholderImage:[UIImage imageNamed:@"default_user_icon"] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
    }];
    
    cell.backImageView.layer.cornerRadius=10;
    cell.backImageView.clipsToBounds=YES;
    
    cell.ImageProduct.layer.cornerRadius=10;
    cell.ImageProduct.clipsToBounds=YES;
    
    CAGradientLayer *gradient = [CAGradientLayer layer];
    
    gradient.frame = cell.backTrackView.bounds;
    gradient.colors = [NSArray arrayWithObjects:
                       (id)[UIColor colorWithRed:52.0f/255.0f green:37.0f/255.0f blue:175.0f/255.0f alpha:1.0f].CGColor,
                       (id)[UIColor colorWithRed:3.0f/255.0f green:110.0f/255.0f blue:216.0f/255.0f alpha:1.0f].CGColor,
                       nil];
    
    [cell.backTrackView.layer insertSublayer:gradient atIndex:0];
    
    cell.backTrackView.layer.cornerRadius=12.5;
    cell.backTrackView.clipsToBounds=true;
    
    return cell;
}

-(void)viewWillAppear:(BOOL)animated{
    [[self navigationController] setNavigationBarHidden:YES animated:YES];
}

@end
