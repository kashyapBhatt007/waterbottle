//
//  CustomerOrdersViewController.h
//  WaterBottle
//
//  Created by Kashyap Bhatt on 29/07/19.
//  Copyright © 2019 Kashyap. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface CustomerOrdersViewController : UIViewController

@property NSMutableArray * tableArray;

@property NSString * order_status;
@property NSString * from_date;
@property NSString * to_date;
@property int page_number;
@property NSString * search_word;

@property NSInteger TotalCount;

@property (weak, nonatomic) IBOutlet UILabel *labelEmpty;
@property (weak, nonatomic) IBOutlet UITableView *table;

@end

NS_ASSUME_NONNULL_END
