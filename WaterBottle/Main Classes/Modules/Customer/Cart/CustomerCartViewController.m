//
//  CustomerCartViewController.m
//  WaterBottle
//
//  Created by Kashyap Bhatt on 01/08/19.
//  Copyright © 2019 Kashyap. All rights reserved.
//

#import "CustomerCartViewController.h"
#import "KBAPIService.h"
#import "KBSpinner.h"
#import "Internet.h"
#import "KBAlert.h"
#import "CustomerOrderTableViewCell.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import "CustomerBuyNowViewController.h"

@interface CustomerCartViewController ()

@end

@implementation CustomerCartViewController

-(void)viewWillAppear:(BOOL)animated
{
    _labelEmpty.hidden=true;
    _backViewForProceedButton.hidden=YES;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    _labelEmpty.text=@"Opps..!\nLooks like you don't have anything in your cart.";
    
    CAGradientLayer *gradient = [CAGradientLayer layer];
    
    gradient.frame = _backViewForProceedButton.bounds;
    gradient.colors = [NSArray arrayWithObjects:
                       (id)[UIColor colorWithRed:52.0f/255.0f green:37.0f/255.0f blue:175.0f/255.0f alpha:1.0f].CGColor,
                       (id)[UIColor colorWithRed:3.0f/255.0f green:110.0f/255.0f blue:216.0f/255.0f alpha:1.0f].CGColor,
                       nil];
    
    [_backViewForProceedButton.layer insertSublayer:gradient atIndex:0];

    _backViewForProceedButton.layer.cornerRadius=20;
    _backViewForProceedButton.clipsToBounds=YES;
}

-(void)viewDidAppear:(BOOL)animated
{
    [self InitializeValues];
    
    [KBSpinner show];
    
    [self CallApi];
}

-(void)InitializeValues
{
    _tableArray=[[NSMutableArray alloc]init];
}

-(void)CallApi
{
    //GetCustomerCartDetailswithDelegate
    _table.hidden=YES;
    
    [KBAPIService GetCustomerCartDetailswithDelegate:self onSuccessCallback:@selector(GotCartDetails:) onFailureCallback:@selector(ErrorResponse:)];
}

-(void)GotCartDetails:(id)response
{
    NSLog(@"%@",response);
    NSString * status=[[NSString alloc]initWithFormat:@"%@",[response objectForKey:@"status"]];
    
    [KBSpinner dismiss];
    if ([status isEqualToString:@"1"])
    {
        _tableArray=[[response objectForKey:@"productInCart"] mutableCopy];
        
        NSString * badgeValueToSet=[[NSString alloc]initWithFormat:@"%lu",(unsigned long)_tableArray.count];
        
        if ([badgeValueToSet isEqualToString:@"0"])
        {
            [self.tabBarItem setBadgeValue:nil];
        }
        else{
            [self.tabBarItem setBadgeValue:badgeValueToSet];
        }
        
        if (_tableArray.count==0)
        {
            _backViewForProceedButton.hidden=YES;
            _table.hidden=YES;
            _labelEmpty.hidden=false;
        }
        else
        {
            _table.hidden=NO;
            _labelEmpty.hidden=true;
            
            _backViewForProceedButton.hidden=NO;
        }
        
        [_table reloadData];
    }
    else
    {
        
    }
}

-(void)ErrorResponse:(id)response
{
    NSLog(@"%@",response);
    [KBSpinner dismiss];
}

// tableview methods
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return _tableArray.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *simpleTableIdentifier = @"CustomerOrderTableViewCell";
    
    CustomerOrderTableViewCell *cell = (CustomerOrderTableViewCell *)[tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
    
    if (cell == nil)
    {
        NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"CustomerOrderTableViewCell" owner:self options:nil];
        cell = [nib objectAtIndex:0];
    }
    
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    NSMutableDictionary * oneDict=[_tableArray objectAtIndex:indexPath.row];

    CAGradientLayer *gradient1 = [CAGradientLayer layer];
    
    gradient1.frame = cell.backViewAddProduct.bounds;
    gradient1.colors = [NSArray arrayWithObjects:
                       (id)[UIColor colorWithRed:52.0f/255.0f green:37.0f/255.0f blue:175.0f/255.0f alpha:1.0f].CGColor,
                       (id)[UIColor colorWithRed:3.0f/255.0f green:110.0f/255.0f blue:216.0f/255.0f alpha:1.0f].CGColor,
                       nil];
    
    [cell.backViewAddProduct.layer insertSublayer:gradient1 atIndex:0];
    
    CAGradientLayer *gradient2 = [CAGradientLayer layer];
    
    gradient2.frame = cell.backViewForQuantity.bounds;
    gradient2.colors = [NSArray arrayWithObjects:
                       (id)[UIColor colorWithRed:52.0f/255.0f green:37.0f/255.0f blue:175.0f/255.0f alpha:1.0f].CGColor,
                       (id)[UIColor colorWithRed:3.0f/255.0f green:110.0f/255.0f blue:216.0f/255.0f alpha:1.0f].CGColor,
                       nil];
    
    [cell.backViewForQuantity.layer insertSublayer:gradient2 atIndex:0];
    
    CAGradientLayer *gradient = [CAGradientLayer layer];
    
    gradient.frame = cell.backViewForReturnQuantity.bounds;
    gradient.colors = [NSArray arrayWithObjects:
                       (id)[UIColor colorWithRed:52.0f/255.0f green:37.0f/255.0f blue:175.0f/255.0f alpha:1.0f].CGColor,
                       (id)[UIColor colorWithRed:3.0f/255.0f green:110.0f/255.0f blue:216.0f/255.0f alpha:1.0f].CGColor,
                       nil];
    
    [cell.backViewForReturnQuantity.layer insertSublayer:gradient atIndex:0];
    
    cell.backViewAddProduct.layer.cornerRadius=15;
    cell.backViewAddProduct.clipsToBounds=YES;
    cell.backViewForQuantity.layer.cornerRadius=15;
    cell.backViewForQuantity.clipsToBounds=YES;
    cell.backViewForReturnQuantity.layer.cornerRadius=15;
    cell.backViewForReturnQuantity.clipsToBounds=YES;
    
    cell.labelProductName.text=[[NSString alloc]initWithFormat:@"%@",[oneDict objectForKey:@"product_name"]];
    cell.labelMeasurements.text=[[NSString alloc]initWithFormat:@"%@ Units in 1 Case",[oneDict objectForKey:@"units_in_one_case"]];
    cell.labelPrice.text=[[NSString alloc]initWithFormat:@"%@",[oneDict objectForKey:@"price"]];
    
    NSURL * FinalURL=[NSURL URLWithString:[oneDict objectForKey:@"product_image"]];
    
    [cell.imageProduct sd_setImageWithURL:FinalURL placeholderImage:[UIImage imageNamed:@"default_user_icon"] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
    }];
    
    cell.imageProduct.layer.cornerRadius=10;
    cell.imageProduct.clipsToBounds=YES;
    
    cell.backViewForProductImage.layer.cornerRadius=10;
    cell.backViewForProductImage.clipsToBounds=YES;
    
    cell.backViewForReturnQuantity.hidden=YES;
    cell.backViewForQuantity.hidden=YES;
    
    cell.buttonCheckbox.tag=indexPath.row;
    [cell.buttonCheckbox addTarget:self action:@selector(SelectDeselect:) forControlEvents:UIControlEventTouchUpInside];
    
    cell.buttonAddToCart.tag=indexPath.row;
    [cell.buttonAddToCart addTarget:self action:@selector(AddToCart:) forControlEvents:UIControlEventTouchUpInside];
    
    cell.buttonDecreaseQuantity.tag=indexPath.row;
    [cell.buttonDecreaseQuantity addTarget:self action:@selector(DecreaseQuantity:) forControlEvents:UIControlEventTouchUpInside];
    
    cell.buttonIncreaseQuantity.tag=indexPath.row;
    [cell.buttonIncreaseQuantity addTarget:self action:@selector(IncreaseQuantity:) forControlEvents:UIControlEventTouchUpInside];
    
    cell.buttonIncreaseReturnQuantity.tag=indexPath.row;
    [cell.buttonIncreaseReturnQuantity addTarget:self action:@selector(IncreaseReturnQuantity:) forControlEvents:UIControlEventTouchUpInside];
    
    cell.buttonDecreaseReturnQuantity.tag=indexPath.row;
    [cell.buttonDecreaseReturnQuantity addTarget:self action:@selector(DecreaseReturnQuantity:) forControlEvents:UIControlEventTouchUpInside];
    
    NSString * quantity=[[NSString alloc]initWithFormat:@"%@",[oneDict objectForKey:@"quantity"]];
    
    cell.labelQuantity.text=quantity;
    
    if ([quantity isEqualToString:@"0"])
    {
        cell.backViewForQuantity.hidden=YES;
        cell.backViewAddProduct.hidden=NO;
        //cell.secondBackView.hidden=YES;
    }
    else{
        cell.backViewForQuantity.hidden=NO;
        cell.backViewAddProduct.hidden=YES;
        //cell.secondBackView.hidden=NO;
    }
    
    NSString * return_quantity=[[NSString alloc]initWithFormat:@"%@",[oneDict objectForKey:@"return_quantity"]];
    
    cell.labelRetunQuantity.text=return_quantity;
    
    if ([return_quantity isEqualToString:@"0"])
    {
        cell.backViewForReturnQuantity.hidden=YES;
        cell.imaegCheckbox.image=[UIImage imageNamed:@"empty_checkbox"];
    }
    else
    {
        cell.backViewForReturnQuantity.hidden=NO;
        cell.imaegCheckbox.image=[UIImage imageNamed:@"filled_checkbox"];
    }
    
    return cell;
}

-(void)SelectDeselect:(UIButton *)myButton
{
    NSMutableDictionary * oneDict=[[_tableArray objectAtIndex:myButton.tag] mutableCopy];
    
    NSString * return_quantity=[[NSString alloc]initWithFormat:@"%@",[oneDict objectForKey:@"return_quantity"]];
    
    if ([return_quantity isEqualToString:@"0"])
    {
        [oneDict setObject:@"1" forKey:@"return_quantity"];
    }
    else{
        [oneDict setObject:@"0" forKey:@"return_quantity"];
    }
    
    [_tableArray replaceObjectAtIndex:myButton.tag withObject:oneDict];
    [_table reloadData];
    
    [self CallProductAPI:oneDict];
}

-(void)AddToCart:(UIButton *)myButton
{
    NSMutableDictionary * oneDict=[[_tableArray objectAtIndex:myButton.tag] mutableCopy];
    
    NSString * quantity=[[NSString alloc]initWithFormat:@"%@",[oneDict objectForKey:@"quantity"]];
    
    if ([quantity isEqualToString:@"0"])
    {
        [oneDict setObject:@"1" forKey:@"quantity"];
    }
    
    [_tableArray replaceObjectAtIndex:myButton.tag withObject:oneDict];
    [_table reloadData];
    
    [self CallProductAPI:oneDict];
    // AddCustomerProductToCart
}

-(void)DecreaseQuantity:(UIButton *)myButton
{
    NSMutableDictionary * oneDict=[[_tableArray objectAtIndex:myButton.tag] mutableCopy];
    
    NSString * quantity=[[NSString alloc]initWithFormat:@"%@",[oneDict objectForKey:@"quantity"]];
    
    NSInteger quantityInInteger=quantity.integerValue;
    quantityInInteger=quantityInInteger-1;
    
    NSString * newQuantity=[[NSString alloc]initWithFormat:@"%ld",(long)quantityInInteger];
    [oneDict setObject:newQuantity forKey:@"quantity"];
    
    [_tableArray replaceObjectAtIndex:myButton.tag withObject:oneDict];
    
    [_table reloadData];
    
    [self CallProductAPI:oneDict];
}

-(void)IncreaseQuantity:(UIButton *)myButton
{
    NSMutableDictionary * oneDict=[[_tableArray objectAtIndex:myButton.tag] mutableCopy];
    
    NSString * quantity=[[NSString alloc]initWithFormat:@"%@",[oneDict objectForKey:@"quantity"]];
    
    NSInteger quantityInInteger=quantity.integerValue;
    quantityInInteger=quantityInInteger+1;
    
    NSString * newQuantity=[[NSString alloc]initWithFormat:@"%ld",(long)quantityInInteger];
    [oneDict setObject:newQuantity forKey:@"quantity"];
    
    [_tableArray replaceObjectAtIndex:myButton.tag withObject:oneDict];
    
    [_table reloadData];
    
    [self CallProductAPI:oneDict];
}

-(void)DecreaseReturnQuantity:(UIButton *)myButton
{
    NSMutableDictionary * oneDict=[[_tableArray objectAtIndex:myButton.tag] mutableCopy];
    
    NSString * quantity=[[NSString alloc]initWithFormat:@"%@",[oneDict objectForKey:@"return_quantity"]];
    
    NSInteger quantityInInteger=quantity.integerValue;
    quantityInInteger=quantityInInteger-1;
    
    NSString * newQuantity=[[NSString alloc]initWithFormat:@"%ld",(long)quantityInInteger];
    [oneDict setObject:newQuantity forKey:@"return_quantity"];
    
    [_tableArray replaceObjectAtIndex:myButton.tag withObject:oneDict];
    
    [_table reloadData];
    
    [self CallProductAPI:oneDict];
}

-(void)IncreaseReturnQuantity:(UIButton *)myButton
{
    NSMutableDictionary * oneDict=[[_tableArray objectAtIndex:myButton.tag] mutableCopy];
    
    NSString * quantity=[[NSString alloc]initWithFormat:@"%@",[oneDict objectForKey:@"return_quantity"]];
    
    NSInteger quantityInInteger=quantity.integerValue;
    quantityInInteger=quantityInInteger+1;
    
    NSString * newQuantity=[[NSString alloc]initWithFormat:@"%ld",(long)quantityInInteger];
    [oneDict setObject:newQuantity forKey:@"return_quantity"];
    
    [_tableArray replaceObjectAtIndex:myButton.tag withObject:oneDict];
    
    [_table reloadData];
    
    [self CallProductAPI:oneDict];
}

-(void)CallProductAPI:(NSMutableDictionary *)productDict
{
    //+ (void)AddCustomerProductToCart:(NSString *)product_id :(NSString *)quantity :(NSString *)return_quantity
    NSString * product_id=[[NSString alloc]initWithFormat:@"%@",[productDict objectForKey:@"product_id"]];
    
    NSString * quantity=[[NSString alloc]initWithFormat:@"%@",[productDict objectForKey:@"quantity"]];
    
    NSString * return_quantity=[[NSString alloc]initWithFormat:@"%@",[productDict objectForKey:@"return_quantity"]];
    
    [KBAPIService AddCustomerProductToCart:product_id :quantity :return_quantity withDelegate:self onSuccessCallback:@selector(CallApi:) onFailureCallback:@selector(ErrorResponse:)];
}

-(void)CallApi:(id)response
{
    [self InitializeValues];
    _table.hidden=YES;
    [self CallApi];
}

//
- (IBAction)ProceedToBuy:(id)sender
{
    NSUserDefaults * user=[[NSUserDefaults alloc]init];
    [user setObject:@"1" forKey:@"buyNowTableLoad"];
    CustomerBuyNowViewController * CustomerBuyNowView=[[CustomerBuyNowViewController alloc]init];
    CustomerBuyNowView.tableArray=_tableArray;
    [self presentViewController:CustomerBuyNowView animated:YES completion:nil];
}
@end
