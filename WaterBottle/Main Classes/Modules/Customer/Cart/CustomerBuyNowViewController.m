//
//  CustomerBuyNowViewController.m
//  WaterBottle
//
//  Created by Kashyap Bhatt on 02/08/19.
//  Copyright © 2019 Kashyap. All rights reserved.
//

#import "CustomerBuyNowViewController.h"
#import "KBAPIService.h"
#import "KBSpinner.h"
#import "Internet.h"
#import "KBAlert.h"
#import "CustomerBuyNowTableViewCell.h"
#import "TitleNameTableViewCell.h"
#import "CustomerBuyNowSubtotalTableViewCell.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import "CustomerAddressListViewController.h"

@interface CustomerBuyNowViewController ()

@end

@implementation CustomerBuyNowViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

-(void)viewWillAppear:(BOOL)animated
{
    NSUserDefaults * user=[[NSUserDefaults alloc]init];
    
    NSString * buyNowTableLoad=[[NSString alloc]initWithFormat:@"%@",[user objectForKey:@"buyNowTableLoad"]];
    
    if ([buyNowTableLoad isEqualToString:@"1"])
    {
        _table.hidden=YES;
    }
}

-(void)viewDidAppear:(BOOL)animated
{
    NSUserDefaults * user=[[NSUserDefaults alloc]init];
    
    NSString * buyNowTableLoad=[[NSString alloc]initWithFormat:@"%@",[user objectForKey:@"buyNowTableLoad"]];
    
    if ([buyNowTableLoad isEqualToString:@"1"])
    {
        [self setUpTableViewArray];
        
        [_table reloadData];
        
        _table.hidden=NO;
        
        [user setObject:@"0" forKey:@"buyNowTableLoad"];
    }
}

-(void)setUpTableViewArray
{
    _totalCharge=0;
    _deliveryCharge=0;
    _selectedAddress=@"Select Address";
    _selectedAddressId=@"0";
    
    for (int k=0;k<_tableArray.count;k++)
    {
        NSMutableDictionary * oneDict=[[_tableArray objectAtIndex:k]mutableCopy];
        [oneDict setObject:@"2" forKey:@"type"];
        
        NSString * quantity=[[NSString alloc]initWithFormat:@"%@",[oneDict objectForKey:@"quantity"]];
        NSString * price=[[NSString alloc]initWithFormat:@"%@",[oneDict objectForKey:@"price"]];
        
        CGFloat quantityFloat=quantity.floatValue;
        CGFloat priceFloat=price.floatValue;
        
        CGFloat total=quantityFloat * priceFloat;
        _totalCharge +=total;
        
        [_tableArray replaceObjectAtIndex:k withObject:oneDict];
    }

    NSMutableDictionary * titleDict=[[NSMutableDictionary alloc]init];
    [titleDict setObject:@"Buy Now" forKey:@"title"];
    [titleDict setObject:@"1" forKey:@"type"];
    
    [_tableArray insertObject:titleDict atIndex:0];
    
    NSMutableDictionary * chargeValueDict=[[NSMutableDictionary alloc]init];
    [chargeValueDict setObject:@"3" forKey:@"type"];
    
    [_tableArray addObject:chargeValueDict];
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return _tableArray.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSMutableDictionary * oneDict=[_tableArray objectAtIndex:indexPath.row];
    
    NSString * type=[[NSString alloc]initWithFormat:@"%@",[oneDict objectForKey:@"type"]];
    
    if ([type isEqualToString:@"1"])
    {
        static NSString *simpleTableIdentifier = @"TitleNameTableViewCell";
        
        TitleNameTableViewCell *cell = (TitleNameTableViewCell *)[tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
        
        if (cell == nil)
        {
            NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"TitleNameTableViewCell" owner:self options:nil];
            cell = [nib objectAtIndex:0];
        }
        
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        
        cell.labelTitle.text=@"Buy Now";
        
        return cell;
    }
    else if ([type isEqualToString:@"2"])
    {
        static NSString *simpleTableIdentifier = @"CustomerBuyNowTableViewCell";
        
        CustomerBuyNowTableViewCell *cell = (CustomerBuyNowTableViewCell *)[tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
        
        if (cell == nil)
        {
            NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"CustomerBuyNowTableViewCell" owner:self options:nil];
            cell = [nib objectAtIndex:0];
        }
        
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        
        cell.labelProductName.text=[[NSString alloc]initWithFormat:@"%@",[oneDict objectForKey:@"product_name"]];
        cell.labelMeasurements.text=[[NSString alloc]initWithFormat:@"%@ Units in 1 Case",[oneDict objectForKey:@"units_in_one_case"]];
        cell.labelPrice.text=[[NSString alloc]initWithFormat:@"%@",[oneDict objectForKey:@"price"]];
        
        NSURL * FinalURL=[NSURL URLWithString:[oneDict objectForKey:@"product_image"]];
        
        [cell.imageProduct sd_setImageWithURL:FinalURL placeholderImage:[UIImage imageNamed:@"default_user_icon"] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
        }];
        
        cell.imageProduct.layer.cornerRadius=10;
        cell.imageProduct.clipsToBounds=YES;
        
        cell.backViewForProductImage.layer.cornerRadius=10;
        cell.backViewForProductImage.clipsToBounds=YES;
        
        NSString * quantity=[[NSString alloc]initWithFormat:@"Quantity - %@",[oneDict objectForKey:@"quantity"]];
        
        cell.labelQuantity.text=quantity;
        
        NSString * return_quantity=[[NSString alloc]initWithFormat:@"Return Quantity - %@",[oneDict objectForKey:@"return_quantity"]];
        
        cell.labelRetunQuantity.text=return_quantity;
        
        return cell;
    }
    else{
        
        static NSString *simpleTableIdentifier = @"CustomerBuyNowSubtotalTableViewCell";
        
        CustomerBuyNowSubtotalTableViewCell *cell = (CustomerBuyNowSubtotalTableViewCell *)[tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
        
        if (cell == nil)
        {
            NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"CustomerBuyNowSubtotalTableViewCell" owner:self options:nil];
            cell = [nib objectAtIndex:0];
        }
        
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        
        cell.labelSubtotalValue.text=[[NSString alloc]initWithFormat:@"%.2f",_totalCharge];
        cell.labelDeliveryValue.text=[[NSString alloc]initWithFormat:@"%.2f",_deliveryCharge];
        
        cell.labelTotalCharge.text=[[NSString alloc]initWithFormat:@"Total - %.2f",_deliveryCharge + _totalCharge];
        
        if ([_selectedAddress isEqualToString:@"Select Address"])
        {
            cell.labelChange.hidden=YES;
        }
        else
        {
            cell.labelChange.hidden=NO;
        }
        
        cell.labelAddress.text=[[NSString alloc]initWithFormat:@"%@",_selectedAddress];
        
        CAGradientLayer *gradient = [CAGradientLayer layer];
        
        gradient.frame = cell.backViewForProceedButton.bounds;
        gradient.colors = [NSArray arrayWithObjects:
                           (id)[UIColor colorWithRed:52.0f/255.0f green:37.0f/255.0f blue:175.0f/255.0f alpha:1.0f].CGColor,
                           (id)[UIColor colorWithRed:3.0f/255.0f green:110.0f/255.0f blue:216.0f/255.0f alpha:1.0f].CGColor,
                           nil];
        
        [cell.backViewForProceedButton.layer insertSublayer:gradient atIndex:0];
        
        cell.backViewForProceedButton.layer.cornerRadius=20;
        cell.backViewForProceedButton.clipsToBounds=true;
        
        [cell.buttonProceedToBuy addTarget:self action:@selector(ProceedToBuy:) forControlEvents:UIControlEventTouchUpInside];
        
        [cell.buttonChangeAddress addTarget:self action:@selector(SelectAddress:) forControlEvents:UIControlEventTouchUpInside];
        
        cell.labelTotalCharge.layer.cornerRadius=20;
        cell.labelTotalCharge.clipsToBounds=true;
        
        return cell;
    }
}

-(void)ProceedToBuy:(UIButton *)button
{
    if ([_selectedAddressId isEqualToString:@"0"])
    {
        [KBAlert ShowToast:@"Please Select Address" :@""];
    }
    else
    {
        [KBAPIService CustomerProceedToBuy:_selectedAddressId :@"1" :@"" WithDelegate:self onSuccessCallback:@selector(OrderCompleted:) onFailureCallback:@selector(ErrorResponse:)];
    }
}

-(void)OrderCompleted:(id)response
{
    NSString * status=[[NSString alloc]initWithFormat:@"%@",[response objectForKey:@"status"]];
    
    [KBAlert ShowToast:[response objectForKey:@"message"] :@""];
    
    if ([status isEqualToString:@"1"])
    {
        [self dismissViewControllerAnimated:YES completion:^{
            [self.tabBarController setSelectedIndex:1];
        }];
    }
}

-(void)ErrorResponse:(id)response
{
    [KBSpinner dismiss];
    NSLog(@"%@",response);
}

-(void)SelectAddress:(UIButton *)button
{
    CustomerAddressListViewController * CustomerAddressListView =[[CustomerAddressListViewController alloc]init];
    CustomerAddressListView.delegate=self;
    [self presentViewController:CustomerAddressListView animated:true completion:nil];
}

-(void)SelectedAddress:(NSString *)SelectedAddress :(NSString *)SelectedAddressId
{
    _selectedAddress=SelectedAddress;
    _selectedAddressId=SelectedAddressId;
    
    [_table reloadData];
}


- (IBAction)back:(id)sender
{
    [self dismissViewControllerAnimated:YES completion:nil];
}
@end
