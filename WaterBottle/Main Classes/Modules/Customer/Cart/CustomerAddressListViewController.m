//
//  CustomerAddressListViewController.m
//  WaterBottle
//
//  Created by Kashyap Bhatt on 03/08/19.
//  Copyright © 2019 Kashyap. All rights reserved.
//

#import "CustomerAddressListViewController.h"
#import "KBAPIService.h"
#import "KBSpinner.h"
#import "Internet.h"
#import "CustomerAddressTableViewCell.h"
#import "TitleNameTableViewCell.h"

@interface CustomerAddressListViewController ()

@end

@implementation CustomerAddressListViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

-(void)viewWillAppear:(BOOL)animated
{
    _labelNoAddress.hidden=YES;
}

-(void)viewDidAppear:(BOOL)animated
{
    [self InitializeValues];
    
    [KBSpinner show];
    
    _page_number=0;
    [self CallApi];
}

-(void)InitializeValues
{
    _tableArray=[[NSMutableArray alloc]init];
}

-(void)CallApi
{
    [KBAPIService GetCustomerAddress:_page_number WithDelegate:self onSuccessCallback:@selector(GotCustomerAddressList:) onFailureCallback:@selector(ErrorResponse:)];
}

-(void)GotCustomerAddressList:(id)response
{
    NSString * status=[[NSString alloc]initWithFormat:@"%@",[response objectForKey:@"status"]];
    
    NSMutableArray * tempArray=[response objectForKey:@"customerAddresses"];
    
    if ([status isEqualToString:@"1"])
    {
        if (_page_number==0)
        {
            NSString * totalCount=[[NSString alloc]initWithFormat:@"%@",[response objectForKey:@"totalCount"]];
            
            _TotalCount=[totalCount integerValue];
            
            _selectedProductId=[[NSString alloc]initWithFormat:@"%@",[response objectForKey:@"selectedAddressId"]];
            
            if (tempArray.count==0)
            {
                _labelNoAddress.hidden=NO;
            }
            else
            {
                _labelNoAddress.hidden=YES;
            }
        }
        
        for (int k=0;k<tempArray.count;k++)
        {
            [_tableArray addObject:[tempArray objectAtIndex:k]];
        }
        
        [_table reloadData];
    }
    [KBSpinner dismiss];
}

-(void)ErrorResponse:(id)response
{
    [KBSpinner dismiss];
    NSLog(@"%@",response);
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return _tableArray.count+1;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row==0)
    {
        static NSString *simpleTableIdentifier = @"TitleNameTableViewCell";
        
        TitleNameTableViewCell *cell = (TitleNameTableViewCell *)[tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
        
        if (cell == nil)
        {
            NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"TitleNameTableViewCell" owner:self options:nil];
            cell = [nib objectAtIndex:0];
        }
        
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        
        cell.labelTitle.text=@"Addresses";
        
        return cell;
    }
    else
    {
        static NSString *simpleTableIdentifier = @"CustomerAddressTableViewCell";
        
        CustomerAddressTableViewCell *cell = (CustomerAddressTableViewCell *)[tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
        
        if (cell == nil)
        {
            NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"CustomerAddressTableViewCell" owner:self options:nil];
            cell = [nib objectAtIndex:0];
        }
        
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        
        NSMutableDictionary * oneDict=[_tableArray objectAtIndex:indexPath.row-1];
        
        cell.labelCategory.text=[[NSString alloc]initWithFormat:@"%@",[oneDict objectForKey:@"address_category_title"]];
        
        cell.labelAddress.text=[[NSString alloc]initWithFormat:@"%@",[oneDict objectForKey:@"address"]];
        
        cell.backViewCategory.layer.cornerRadius=15;
        cell.backViewCategory.clipsToBounds=YES;
        
        [cell.buttonDelete addTarget:self action:@selector(DeleteAddress:) forControlEvents:UIControlEventTouchUpInside];
        
        cell.buttonDelete.tag=indexPath.row;
        [cell.buttonEdit addTarget:self action:@selector(EditAddress:) forControlEvents:UIControlEventTouchUpInside];
        cell.buttonEdit.tag=indexPath.row;
        return cell;
    }
}

-(void)DeleteAddress:(UIButton *)sender
{
    UIAlertController * alert=[UIAlertController alertControllerWithTitle:nil message:@"Delete Address?" preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction * okAction=[UIAlertAction actionWithTitle:@"Yes" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        NSMutableDictionary * oneDict=[self->_tableArray objectAtIndex:sender.tag-1];
        
        NSString * address_id=[[NSString alloc]initWithFormat:@"%@",[oneDict objectForKey:@"address_id"]];
        [self CallDeleteAddressAPI:address_id];
    }];
    
    UIAlertAction * noAction=[UIAlertAction actionWithTitle:@"No" style:UIAlertActionStyleDefault handler:nil];
    
    [alert addAction:noAction];
    [alert addAction:okAction];
    
    [self presentViewController:alert animated:YES completion:nil];
}

-(void)CallDeleteAddressAPI:(NSString *)address_id
{
    [KBSpinner show];
    [KBAPIService DeleteCustomerAddress:address_id WithDelegate:self onSuccessCallback:@selector(CustomerAddressDeleted:) onFailureCallback:@selector(ErrorResponse:)];
}

-(void)CustomerAddressDeleted:(id)response
{
    NSLog(@"%@",response);
    [KBSpinner dismiss];
    NSString * status=[[NSString alloc]initWithFormat:@"%@",[response objectForKey:@"status"]];
    if ([status isEqualToString:@"1"])
    {
        [self InitializeValues];
        [self CallApi];
    }
}
-(void)EditAddress:(UIButton *)sender
{
    
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSMutableDictionary * oneDict=[_tableArray objectAtIndex:indexPath.row-1];
    
    NSString * address=[oneDict objectForKey:@"address"];
    
    NSString * addressId=[oneDict objectForKey:@"address_id"];
    
    [self.delegate SelectedAddress:address :addressId];
    
    [self dismissViewControllerAnimated:true completion:nil];
}

- (IBAction)AddNewAddress:(id)sender
{
    
}

- (IBAction)back:(id)sender
{
    [self dismissViewControllerAnimated:YES completion:nil];
}
@end
