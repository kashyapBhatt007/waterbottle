//
//  CustomerAddressListViewController.h
//  WaterBottle
//
//  Created by Kashyap Bhatt on 03/08/19.
//  Copyright © 2019 Kashyap. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface CustomerAddressListViewController : UIViewController <UITableViewDelegate,UITableViewDataSource>

@property (weak, nonatomic) IBOutlet UILabel *labelNoAddress;

@property NSInteger TotalCount;
- (IBAction)back:(id)sender;
@property NSString * selectedProductId;
@property int page_number;

- (IBAction)AddNewAddress:(id)sender;
@property NSMutableArray * tableArray;
@property (weak, nonatomic) IBOutlet UITableView *table;
@property id delegate;
@end

@protocol CustomerAddressListViewControllerDelegate <NSObject>

-(void)SelectedAddress:(NSString *)SelectedAddress :(NSString *)SelectedAddressId;

@end
NS_ASSUME_NONNULL_END
