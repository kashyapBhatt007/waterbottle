//
//  CustomerCartViewController.h
//  WaterBottle
//
//  Created by Kashyap Bhatt on 01/08/19.
//  Copyright © 2019 Kashyap. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface CustomerCartViewController : UIViewController
@property (weak, nonatomic) IBOutlet UITableView *table;
@property (weak, nonatomic) IBOutlet UIView *backViewForProceedButton;
- (IBAction)ProceedToBuy:(id)sender;

@property NSMutableArray * tableArray;
@property (weak, nonatomic) IBOutlet UILabel *labelEmpty;
@end

NS_ASSUME_NONNULL_END
