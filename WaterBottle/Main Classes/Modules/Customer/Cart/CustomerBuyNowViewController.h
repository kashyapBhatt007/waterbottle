//
//  CustomerBuyNowViewController.h
//  WaterBottle
//
//  Created by Kashyap Bhatt on 02/08/19.
//  Copyright © 2019 Kashyap. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface CustomerBuyNowViewController : UIViewController
- (IBAction)back:(id)sender;

@property (weak, nonatomic) IBOutlet UITableView *table;

@property NSMutableArray * tableArray;

@property CGFloat totalCharge;
@property CGFloat deliveryCharge;
@property NSString * selectedAddress;
@property NSString * selectedAddressId;

@end

NS_ASSUME_NONNULL_END
