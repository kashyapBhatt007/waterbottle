//
//  CustomerHomeViewController.h
//  WaterBottle
//
//  Created by Kashyap Bhatt on 29/07/19.
//  Copyright © 2019 Kashyap. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface CustomerHomeViewController : UIViewController

@property NSMutableArray * tableArray;
@property NSInteger TotalCount;

@property int page_number;
@property NSString * search_word;



@property NSInteger numberOfProductsInCart;
@property (weak, nonatomic) IBOutlet UILabel *labelEmpty;
@property (weak, nonatomic) IBOutlet UITableView *table;

@end

NS_ASSUME_NONNULL_END
