//
//  DeliveryOrderViewController.m
//  WaterBottle
//
//  Created by Kashyap Bhatt on 09/08/19.
//  Copyright © 2019 Kashyap. All rights reserved.
//

#import "DeliveryOrderViewController.h"
#import "KBAPIService.h"
#import "KBSpinner.h"
#import "Internet.h"
#import "KBAlert.h"
#import "DeliveryOrderTableViewCell.h"
#import <SDWebImage/UIImageView+WebCache.h>

@interface DeliveryOrderViewController ()

@end

@implementation DeliveryOrderViewController

-(void)viewWillAppear:(BOOL)animated
{
    [self.navigationController setNavigationBarHidden:true];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    _labelEmpty.hidden=true;
}

-(void)InitializeValues
{
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(KeyboardUpMethod:) name:UIKeyboardWillShowNotification object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(KeyboardDownMethod:) name:UIKeyboardWillHideNotification object:nil];
    
    _searchBar.delegate=self;
    _tableArray=[[NSMutableArray alloc]init];
    
    _from_date=[[NSString alloc]initWithFormat:@"%@",@""];
    _to_date=[[NSString alloc]initWithFormat:@"%@",@""];
    
    _page_number=0;
    _search_word=[[NSString alloc]initWithFormat:@"%@",@""];
}

-(void)CallApi
{
    [KBSpinner show];
    
    [KBAPIService DeliveryOrderList:_from_date :_to_date :_page_number :_search_word WithDelegate:self onSuccessCallback:@selector(GotList:) onFailureCallback:@selector(ErrorResponse:)];
}

-(void)GotList:(id)response
{
    NSLog(@"%@",response);
    NSString * status=[[NSString alloc]initWithFormat:@"%@",[response objectForKey:@"status"]];
    
    [KBSpinner dismiss];
    if ([status isEqualToString:@"1"])
    {
        NSMutableArray * tempArray=[response objectForKey:@"orderList"];
        
        if (_page_number==0)
        {
            NSString * totalCount=[[NSString alloc]initWithFormat:@"%@",[response objectForKey:@"totalCount"]];
            
            _TotalCount=[totalCount integerValue];
            
            NSString * notificationCount=[[NSString alloc]initWithFormat:@"%@",[response objectForKey:@"notificationCount"]];
            
            if (![notificationCount isEqualToString:@"0"]){
                
                [[[[[self tabBarController] tabBar] items]
                  objectAtIndex:1] setBadgeValue:notificationCount];
            }
            else
            {
                [[[[[self tabBarController] tabBar] items]
                  objectAtIndex:1] setBadgeValue:nil];
            }
            
            if (tempArray.count==0)
            {
                _labelEmpty.hidden=false;
            }
            else
            {
                _labelEmpty.hidden=true;
            }
        }
        
        for (int k=0;k<tempArray.count;k++)
        {
            [_tableArray addObject:tempArray[k]];
        }
        
        [_table reloadData];
    }
    else
    {
        
    }
}

-(void)viewDidAppear:(BOOL)animated
{
    _tableHeight=_table.frame.size.height;
    
    [self InitializeValues];
    
    [self CallApi];
}

-(void)ErrorResponse:(id)response
{
    NSLog(@"%@",response);
}

-(void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
    NSString * InternetAvailability=[Internet CheckInternet];
    if ([InternetAvailability isEqualToString:@"Yes"])
    {
        if (_TotalCount/10>_page_number)
        {
            _page_number++;
            
            [self CallApi];
        }
    }
    else
    {
        [KBAlert ShowInternetToast];
    }
}

// tableview methods
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return _tableArray.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *simpleTableIdentifier = @"DeliveryOrderTableViewCell";
    
    DeliveryOrderTableViewCell *cell = (DeliveryOrderTableViewCell *)[tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
    
    if (cell == nil)
    {
        NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"DeliveryOrderTableViewCell" owner:self options:nil];
        cell = [nib objectAtIndex:0];
    }
    
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    [cell.BackView.layer setCornerRadius:10.0f];
    [cell.BackView.layer setShadowColor:[UIColor groupTableViewBackgroundColor].CGColor];
    [cell.BackView.layer setShadowOpacity:2.0];
    [cell.BackView.layer setShadowRadius:5.0];
    [cell.BackView.layer setShadowOffset:CGSizeMake(0.0f, 2.0f)];
    
    NSMutableDictionary * oneDict=[_tableArray objectAtIndex:indexPath.row];
    cell.LabelId.text=[[NSString alloc]initWithFormat:@"ID : %@",[oneDict objectForKey:@"order_number"]];
    cell.labelAddress.text=[[NSString alloc]initWithFormat:@"%@",[oneDict objectForKey:@"billing_address"]];
    cell.labelAssignedTo.text=[[NSString alloc]initWithFormat:@"%@",[oneDict objectForKey:@"completed_status_title"]];
    
    NSURL * FinalURL=[NSURL URLWithString:[oneDict objectForKey:@"order_image_icon"]];
    
    [cell.ImageProduct sd_setImageWithURL:FinalURL placeholderImage:[UIImage imageNamed:@"default_user_icon"] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
    }];
    
    cell.backImageView.layer.cornerRadius=10;
    cell.backImageView.clipsToBounds=YES;
    
    cell.ImageProduct.layer.cornerRadius=10;
    cell.ImageProduct.clipsToBounds=YES;
    
    return cell;
}

-(void)KeyboardUpMethod:(NSNotification *)notification
{
    CGFloat bottomPadding = 0;
    
    if (@available(iOS 11.0, *)) {
        UIWindow *window = UIApplication.sharedApplication.keyWindow;
        bottomPadding = window.safeAreaInsets.bottom;
    }
    
    NSDictionary* keyboardInfo = [notification userInfo];
    NSValue* keyboardFrameBegin = [keyboardInfo valueForKey:UIKeyboardFrameEndUserInfoKey];
    CGRect keyboardFrameBeginRect = [keyboardFrameBegin CGRectValue];
    
    [_table setFrame:CGRectMake(0, self.table.frame.origin.y, self.table.frame.size.width, _tableHeight-keyboardFrameBeginRect.size.height-bottomPadding)];
}

- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar
{
    [_searchBar resignFirstResponder];
}

-(void)KeyboardDownMethod:(NSNotification *)notification
{
    [_table setFrame:CGRectMake(0, self.table.frame.origin.y, self.table.frame.size.width, _tableHeight)];
}

-(void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText
{
    _page_number=0;
    _search_word=[[NSString alloc]initWithFormat:@"%@",searchText];
    
    [_tableArray removeAllObjects];
    [_table reloadData];
    
    [self CallApi];
}
@end
