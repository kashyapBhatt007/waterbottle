//
//  DeliveryOrderViewController.h
//  WaterBottle
//
//  Created by Kashyap Bhatt on 09/08/19.
//  Copyright © 2019 Kashyap. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface DeliveryOrderViewController : UIViewController <UISearchBarDelegate>

@property CGFloat tableHeight;

@property (weak, nonatomic) IBOutlet UISearchBar *searchBar;

@property NSMutableArray * tableArray;

@property NSString * from_date;
@property NSString * to_date;
@property int page_number;
@property NSString * search_word;

@property NSInteger TotalCount;

@property (weak, nonatomic) IBOutlet UILabel *labelEmpty;
@property (weak, nonatomic) IBOutlet UITableView *table;

@end

NS_ASSUME_NONNULL_END
