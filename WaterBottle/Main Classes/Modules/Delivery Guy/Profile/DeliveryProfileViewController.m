//
//  DeliveryProfileViewController.m
//  WaterBottle
//
//  Created by Kashyap Bhatt on 15/08/19.
//  Copyright © 2019 Kashyap. All rights reserved.
//

#import "DeliveryProfileViewController.h"
#import "KBAPIService.h"
#import "KBSpinner.h"
#import "Internet.h"
#import "KBAlert.h"

@interface DeliveryProfileViewController ()

@end

@implementation DeliveryProfileViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

- (IBAction)Logout:(id)sender
{
    UIAlertController * alert=[UIAlertController alertControllerWithTitle:@"Do you want to Logout?" message:@"" preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction * okAction=[UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        
        [KBSpinner show];
        
        [KBAPIService LogoutUserWithDelegate:self onSuccessCallback:@selector(LoggedoutSuccessfully:) onFailureCallback:@selector(ErrorResponse:)];
    }];
    
    UIAlertAction * cancelAction=[UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:nil];
    
    [alert addAction:okAction];
    [alert addAction:cancelAction];
    [self presentViewController:alert animated:YES completion:nil];
}

-(void)LoggedoutSuccessfully:(id)response
{
    NSLog(@"%@",response);
    NSString * status=[[NSString alloc]initWithFormat:@"%@",[response objectForKey:@"status"]];
    [KBSpinner dismiss];
    
    if ([status isEqualToString:@"1"])
    {
        NSUserDefaults * user=[[NSUserDefaults alloc]init];
        [user setObject:@"0" forKey:@"LoggedIn"];
        
        [super.navigationController popToRootViewControllerAnimated:YES];
    }
}

-(void)ErrorResponse:(id)response
{
    NSLog(@"%@",response);
}
@end
