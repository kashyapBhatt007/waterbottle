//
//  DeliveryNotificationViewController.h
//  WaterBottle
//
//  Created by Kashyap Bhatt on 09/08/19.
//  Copyright © 2019 Kashyap. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface DeliveryNotificationViewController : UIViewController

@property int page_number;
@property NSInteger TotalCount;
@property NSMutableArray * tableArray;

@property (weak, nonatomic) IBOutlet UITableView *table;
@property NSString * orderAcceptRejectString;
@property (weak, nonatomic) IBOutlet UILabel *labelEmpty;

@end

NS_ASSUME_NONNULL_END
