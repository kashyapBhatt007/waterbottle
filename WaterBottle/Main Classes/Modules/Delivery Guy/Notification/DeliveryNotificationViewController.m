//
//  DeliveryNotificationViewController.m
//  WaterBottle
//
//  Created by Kashyap Bhatt on 09/08/19.
//  Copyright © 2019 Kashyap. All rights reserved.
//

#import "DeliveryNotificationViewController.h"
#import "KBAPIService.h"
#import "KBSpinner.h"
#import "Internet.h"
#import "KBAlert.h"
#import "DeliveryNotificationTableViewCell.h"
#import <SDWebImage/UIImageView+WebCache.h>

@interface DeliveryNotificationViewController ()

@end

@implementation DeliveryNotificationViewController

-(void)viewDidAppear:(BOOL)animated{
    [self InitializeValues];
    
    [self CallApi];
}
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    _labelEmpty.hidden=true;
}

-(void)InitializeValues
{
    _tableArray=[[NSMutableArray alloc]init];
    _page_number=0;
}

- (IBAction)GoToFilter:(id)sender
{
    
}

-(void)SelectedFilters:(NSDictionary *)SelectedFilters
{
    
}

-(void)GotList:(id)response
{
    NSLog(@"%@",response);
    NSString * status=[[NSString alloc]initWithFormat:@"%@",[response objectForKey:@"status"]];
    
    [KBSpinner dismiss];
    if ([status isEqualToString:@"1"])
    {
        NSMutableArray * tempArray=[response objectForKey:@"orderList"];
        
        if (_page_number==0)
        {
            NSString * totalCount=[[NSString alloc]initWithFormat:@"%@",[response objectForKey:@"totalCount"]];
            
            _TotalCount=[totalCount integerValue];
            
            if (tempArray.count==0)
            {
                _labelEmpty.hidden=false;
            }
            else
            {
                _labelEmpty.hidden=true;
            }
        }
        
        for (int k=0;k<tempArray.count;k++)
        {
            [_tableArray addObject:tempArray[k]];
        }
        
        [_table reloadData];
    }
    else
    {
        
    }
}

-(void)ErrorResponse:(id)response
{
    NSLog(@"%@",response);
}

-(void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
    NSString * InternetAvailability=[Internet CheckInternet];
    if ([InternetAvailability isEqualToString:@"Yes"])
    {
        if (_TotalCount/10>_page_number)
        {
            _page_number++;
            
            [self CallApi];
        }
    }
    else
    {
        [KBAlert ShowInternetToast];
    }
}

-(void)CallApi
{
    [KBSpinner show];
     
    [KBAPIService GetDeliveryNotificationList:_page_number WithDelegate:self onSuccessCallback:@selector(GotList:) onFailureCallback:@selector(ErrorResponse:)];
}

// tableview methods
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return _tableArray.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *simpleTableIdentifier = @"DeliveryNotificationTableViewCell";
    
    DeliveryNotificationTableViewCell *cell = (DeliveryNotificationTableViewCell *)[tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
    
    if (cell == nil)
    {
        NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"DeliveryNotificationTableViewCell" owner:self options:nil];
        cell = [nib objectAtIndex:0];
    }
    
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    NSMutableDictionary * oneDict=[_tableArray objectAtIndex:indexPath.row];

    cell.labelid.text=[[NSString alloc]initWithFormat:@"ID : %@",[oneDict objectForKey:@"order_number"]];

    cell.labelAddress.text=[[NSString alloc]initWithFormat:@"%@",[oneDict objectForKey:@"billing_address"]];
    
    [cell.BackView.layer setCornerRadius:10.0f];
    [cell.BackView.layer setShadowColor:[UIColor groupTableViewBackgroundColor].CGColor];
    [cell.BackView.layer setShadowOpacity:2.0];
    [cell.BackView.layer setShadowRadius:5.0];
    [cell.BackView.layer setShadowOffset:CGSizeMake(0.0f, 2.0f)];
    
    CAGradientLayer *gradient1 = [CAGradientLayer layer];
    
    cell.backAccept.layer.cornerRadius=12.5;
    cell.backAccept.clipsToBounds=true;
    
    cell.backDecline.layer.cornerRadius=12.5;
    cell.backDecline.clipsToBounds=true;
    
    gradient1.frame = cell.backAccept.bounds;
    gradient1.colors = [NSArray arrayWithObjects:
                        (id)[UIColor colorWithRed:52.0f/255.0f green:37.0f/255.0f blue:175.0f/255.0f alpha:1.0f].CGColor,
                        (id)[UIColor colorWithRed:3.0f/255.0f green:110.0f/255.0f blue:216.0f/255.0f alpha:1.0f].CGColor,
                        nil];
    
    [cell.backAccept.layer insertSublayer:gradient1 atIndex:0];
    
    cell.backDecline.layer.borderColor=[UIColor lightGrayColor].CGColor;
    cell.backDecline.layer.borderWidth=1;
    
    [cell.buttonAccept addTarget:self action:@selector(OrderApproved:) forControlEvents:UIControlEventTouchUpInside];
    cell.buttonAccept.tag=indexPath.row;
    
    [cell.buttonDecline addTarget:self action:@selector(OrderRejected:) forControlEvents:UIControlEventTouchUpInside];
    cell.buttonDecline.tag=indexPath.row;
    
    NSURL * FinalURL=[NSURL URLWithString:[oneDict objectForKey:@"order_image_icon"]];
    
    [cell.ImageProduct sd_setImageWithURL:FinalURL placeholderImage:[UIImage imageNamed:@"default_user_icon"] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
    }];
    
    cell.backProductImage.layer.cornerRadius=10;
    cell.backProductImage.clipsToBounds=YES;
    
    cell.ImageProduct.layer.cornerRadius=10;
    cell.ImageProduct.clipsToBounds=YES;
    
    return cell;
}

-(void)CallApproveRejectAPI:(NSString *)order_id :(NSString *)approve_reject_status
{
    [KBSpinner show];
    
    NSString * rejected_reason=[[NSString alloc]init];
    
    [KBAPIService DeliveryNotificationApproveReject:order_id :approve_reject_status :rejected_reason WithDelegate:self onSuccessCallback:@selector(OrderApprovedRejected:) onFailureCallback:@selector(ErrorResponse:)];
}

-(void)OrderApprovedRejected:(id)response
{
    NSString * status=[[NSString alloc]initWithFormat:@"%@",[response objectForKey:@"status"]];
    [KBSpinner dismiss];
    if ([status isEqualToString:@"1"])
    {
        if ([_orderAcceptRejectString isEqualToString:@"Accept"])
        {
            [KBAlert ShowToast:@"Order Accepted successfully." :@""];
        }
        else
        {
            [KBAlert ShowToast:@"Order Declined successfully." :@""];
        }
        
        
        [self InitializeValues];
        
        [self CallApi];
    }
}

-(void)OrderApproved:(UIButton *)sender
{
    UIAlertController * alert=[UIAlertController alertControllerWithTitle:nil message:@"Accept Order?" preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction * okAction=[UIAlertAction actionWithTitle:@"Yes" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        NSMutableDictionary * oneDict=[self->_tableArray objectAtIndex:sender.tag];
        NSString * order_id=[[NSString alloc]initWithFormat:@"%@",[oneDict objectForKey:@"order_id"]];
 
        self.orderAcceptRejectString=@"Accept";
        
        [self CallApproveRejectAPI:order_id :@"1" ];
    }];
    
    UIAlertAction * noAction=[UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleDefault handler:nil];
    
    [alert addAction:noAction];
    [alert addAction:okAction];
    
    [self presentViewController:alert animated:YES completion:nil];
}

-(void)OrderRejected:(UIButton *)sender
{
    UIAlertController * alert=[UIAlertController alertControllerWithTitle:nil message:@"Decline Order?" preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction * okAction=[UIAlertAction actionWithTitle:@"Yes" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        NSMutableDictionary * oneDict=[self->_tableArray objectAtIndex:sender.tag];
        NSString * order_id=[[NSString alloc]initWithFormat:@"%@",[oneDict objectForKey:@"order_id"]];
        
        self.orderAcceptRejectString=@"Reject";
        
        [self CallApproveRejectAPI:order_id :@"2" ];
    }];
    
    UIAlertAction * noAction=[UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleDefault handler:nil];
    
    [alert addAction:noAction];
    [alert addAction:okAction];
    
    [self presentViewController:alert animated:YES completion:nil];
}

@end
