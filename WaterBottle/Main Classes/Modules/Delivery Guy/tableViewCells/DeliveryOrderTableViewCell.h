//
//  DeliveryOrderTableViewCell.h
//  WaterBottle
//
//  Created by Kashyap Bhatt on 15/08/19.
//  Copyright © 2019 Kashyap. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface DeliveryOrderTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIButton *navigate;
@property (weak, nonatomic) IBOutlet UIImageView *ImageProduct;
@property (weak, nonatomic) IBOutlet UILabel *LabelId;

@property (weak, nonatomic) IBOutlet UILabel *labelAssignedTo;
@property (weak, nonatomic) IBOutlet UILabel *labelAddress;
@property (weak, nonatomic) IBOutlet UIView *BackView;
@property (weak, nonatomic) IBOutlet UIView *backImageView;

@end

NS_ASSUME_NONNULL_END
