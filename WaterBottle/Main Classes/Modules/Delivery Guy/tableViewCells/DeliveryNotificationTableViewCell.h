//
//  DeliveryNotificationTableViewCell.h
//  WaterBottle
//
//  Created by Kashyap Bhatt on 15/08/19.
//  Copyright © 2019 Kashyap. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface DeliveryNotificationTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIView *BackView;
@property (weak, nonatomic) IBOutlet UILabel *labelAddress;
@property (weak, nonatomic) IBOutlet UIView *backAccept;
@property (weak, nonatomic) IBOutlet UIButton *buttonAccept;
@property (weak, nonatomic) IBOutlet UIButton *buttonDecline;
@property (weak, nonatomic) IBOutlet UIImageView *ImageProduct;

@property (weak, nonatomic) IBOutlet UIView *backDecline;
@property (weak, nonatomic) IBOutlet UILabel *labelid;
@property (weak, nonatomic) IBOutlet UIView *backProductImage;
@end

NS_ASSUME_NONNULL_END
