//
//  DeliveryTabbarViewController.m
//  WaterBottle
//
//  Created by Kashyap Bhatt on 09/08/19.
//  Copyright © 2019 Kashyap. All rights reserved.
//

#import "DeliveryTabbarViewController.h"
#import "DeliveryOrderViewController.h"
#import "DeliveryNotificationViewController.h"
#import "DeliveryProfileViewController.h"

@interface DeliveryTabbarViewController ()

@end

@implementation DeliveryTabbarViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    DeliveryOrderViewController * view1=[[DeliveryOrderViewController alloc]init];
    DeliveryNotificationViewController * view=[[DeliveryNotificationViewController alloc]init];
    DeliveryProfileViewController * view2=[[DeliveryProfileViewController alloc]init];
    
    NSMutableArray *tabViewControllers = [[NSMutableArray alloc] init];
    [tabViewControllers addObject:view1];
    [tabViewControllers addObject:view];
    [tabViewControllers addObject:view2];
    
    [self setViewControllers:tabViewControllers];
    
    self.tabBarItem.imageInsets = UIEdgeInsetsMake(16, 10, -16, 10);
    
    [[UITabBar appearance] setBarTintColor:[UIColor whiteColor]];
    
    // setting up labels and icons for tab bar

    // for orders
    UIImage * MyProjectSelected1=[UIImage imageNamed:@"tab_order_selected"];
    view1.tabBarItem.selectedImage = [MyProjectSelected1
                                      imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    [view1.tabBarItem setImageInsets:UIEdgeInsetsMake(5, 0, -5, 0)];
    view1.tabBarItem.image = [[UIImage imageNamed:@"tab_order_unselected"]
                              imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    view1.tabBarItem.title=@"Orders";
    view1.tabBarItem.tag=1;
    
    // Notifications
    UIImage * MyProjectSelected=[UIImage imageNamed:@"tab_notification_selected"];
    view.tabBarItem.selectedImage = [MyProjectSelected
                                      imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    [view.tabBarItem setImageInsets:UIEdgeInsetsMake(5, 0, -5, 0)];
    view.tabBarItem.image = [[UIImage imageNamed:@"tab_notification_unselected"]
                              imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    view.tabBarItem.title=@"Notifications";
    view.tabBarItem.tag=0;
    
    // profile
    UIImage * MyProjectSelected2=[UIImage imageNamed:@"tab_profile_selected"];
    view2.tabBarItem.selectedImage = [MyProjectSelected2
                                     imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    [view2.tabBarItem setImageInsets:UIEdgeInsetsMake(5, 0, -5, 0)];
    view2.tabBarItem.image = [[UIImage imageNamed:@"tab_profile_unselected"]
                             imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    view2.tabBarItem.title=@"Profile";
    view2.tabBarItem.tag=0;
    
    self.selectedIndex=0;
}


@end
