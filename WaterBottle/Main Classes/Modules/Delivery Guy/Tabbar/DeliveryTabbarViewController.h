//
//  DeliveryTabbarViewController.h
//  WaterBottle
//
//  Created by Kashyap Bhatt on 09/08/19.
//  Copyright © 2019 Kashyap. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface DeliveryTabbarViewController : UITabBarController

@end

NS_ASSUME_NONNULL_END
