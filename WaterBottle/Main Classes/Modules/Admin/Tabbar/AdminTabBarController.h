//
//  AdminTabBarController.h
//  WaterBottle
//
//  Created by Kashyap Bhatt on 22/07/19.
//  Copyright © 2019 Kashyap. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface AdminTabBarController : UITabBarController

@end

NS_ASSUME_NONNULL_END
