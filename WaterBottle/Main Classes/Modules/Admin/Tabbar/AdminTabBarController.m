//
//  AdminTabBarController.m
//  WaterBottle
//
//  Created by Kashyap Bhatt on 22/07/19.
//  Copyright © 2019 Kashyap. All rights reserved.
//

#import "AdminTabBarController.h"
#import "AdminOrderViewController.h"
#import "AdminNotificationsViewController.h"
#import "AdminProfileViewController.h"

@interface AdminTabBarController ()

@end

@implementation AdminTabBarController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    AdminOrderViewController * view=[[AdminOrderViewController alloc]init];
    AdminNotificationsViewController * view1=[[AdminNotificationsViewController alloc]init];
    AdminProfileViewController * view2=[[AdminProfileViewController alloc]init];
    
    NSMutableArray *tabViewControllers = [[NSMutableArray alloc] init];
    [tabViewControllers addObject:view];
    [tabViewControllers addObject:view1];
    [tabViewControllers addObject:view2];
    
    [self setViewControllers:tabViewControllers];
    
    self.tabBarItem.imageInsets = UIEdgeInsetsMake(16, 10, -16, 10);
    
    [[UITabBar appearance] setBarTintColor:[UIColor whiteColor]];
    
    // setting up labels and icons for tab bar
    
    // orders
    UIImage * MyProjectSelected=[UIImage imageNamed:@"tab_order_selected"];
    view.tabBarItem.selectedImage = [MyProjectSelected
                                     imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    [view.tabBarItem setImageInsets:UIEdgeInsetsMake(5, 0, -5, 0)];
    view.tabBarItem.image = [[UIImage imageNamed:@"tab_order_unselected"]
                             imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    view.tabBarItem.title=@"Orders";
    view.tabBarItem.tag=0;
    
    // Notifications
    UIImage * MyProjectSelected1=[UIImage imageNamed:@"tab_notification_selected"];
    view1.tabBarItem.selectedImage = [MyProjectSelected1
                                     imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    [view1.tabBarItem setImageInsets:UIEdgeInsetsMake(5, 0, -5, 0)];
    view1.tabBarItem.image = [[UIImage imageNamed:@"tab_notification_unselected"]
                             imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    view1.tabBarItem.title=@"Notifications";
    view1.tabBarItem.tag=0;
    
    // for profile
    UIImage * MyProjectSelected2=[UIImage imageNamed:@"tab_profile_selected"];
    view2.tabBarItem.selectedImage = [MyProjectSelected2
                                      imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    [view2.tabBarItem setImageInsets:UIEdgeInsetsMake(5, 0, -5, 0)];
    view2.tabBarItem.image = [[UIImage imageNamed:@"tab_profile_unselected"]
                              imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    view2.tabBarItem.title=@"Profile";
    view.tabBarItem.tag=3;
    
    self.selectedIndex=0;   
}

@end
