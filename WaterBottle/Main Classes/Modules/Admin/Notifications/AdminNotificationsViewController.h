//
//  AdminNotificationsViewController.h
//  WaterBottle
//
//  Created by Kashyap Bhatt on 05/08/19.
//  Copyright © 2019 Kashyap. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface AdminNotificationsViewController : UIViewController <UISearchBarDelegate>

@property (weak, nonatomic) IBOutlet UITableView *table;

@property NSMutableArray * tableArray;
@property (weak, nonatomic) IBOutlet UILabel *labelEmpty;
- (IBAction)GoToFilter:(id)sender;
@property (weak, nonatomic) IBOutlet UISearchBar *SearchBar;
@property NSString * selectedOrderStatus;
@property NSString * delivery_user_id;
@property NSString * from_date;
@property NSString * to_date;
@property NSString * order_status;
@property CGFloat tableHeight;
@property NSString * search_word;
@property int page_number;
@property NSInteger TotalCount;

@end

NS_ASSUME_NONNULL_END
