//
//  AdminNotificationsViewController.m
//  WaterBottle
//
//  Created by Kashyap Bhatt on 05/08/19.
//  Copyright © 2019 Kashyap. All rights reserved.
//

#import "AdminNotificationsViewController.h"
#import "KBAPIService.h"
#import "KBSpinner.h"
#import "Internet.h"
#import "KBAlert.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import "AdminNotificationTableViewCell.h"
#import "AdminOrderFilterViewController.h"

@interface AdminNotificationsViewController ()

@end

@implementation AdminNotificationsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    [self InitializeValues];
    
    _labelEmpty.hidden=true;
}

-(void)viewDidAppear:(BOOL)animated
{
    _tableHeight=_table.frame.size.height;
    
    [_tableArray removeAllObjects];
    [_table reloadData];
    
    [self CallApi];
}

-(void)InitializeValues
{
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(KeyboardUpMethod:) name:UIKeyboardWillShowNotification object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(KeyboardDownMethod:) name:UIKeyboardWillHideNotification object:nil];
    
    _SearchBar.delegate=self;
    _tableArray=[[NSMutableArray alloc]init];
    
    _delivery_user_id=[[NSString alloc]initWithFormat:@"%@",@"0"];
    _from_date=[[NSString alloc]initWithFormat:@"%@",@""];
    _to_date=[[NSString alloc]initWithFormat:@"%@",@""];
    _order_status=[[NSString alloc]initWithFormat:@"%@",@"0"];
    _page_number=0;
    _search_word=[[NSString alloc]initWithFormat:@"%@",@""];
}

- (IBAction)GoToFilter:(id)sender
{
    AdminOrderFilterViewController * AdminOrderFilterView=[[AdminOrderFilterViewController alloc]init];
    AdminOrderFilterView.delegate=self;
    AdminOrderFilterView.order_notification_payment=[[NSString alloc]initWithFormat:@"%@",@"2"];
    [self.navigationController pushViewController:AdminOrderFilterView animated:true];
}

-(void)SelectedFilters:(NSDictionary *)SelectedFilters
{
    [_tableArray removeAllObjects];
    [_table reloadData];
    
    _delivery_user_id=[[NSString alloc]initWithFormat:@"%@",[SelectedFilters objectForKey:@"executive"]];
    _from_date=[[NSString alloc]initWithFormat:@"%@",[SelectedFilters objectForKey:@"fromDate"]];
    _to_date=[[NSString alloc]initWithFormat:@"%@",[SelectedFilters objectForKey:@"toDate"]];
    _order_status=[[NSString alloc]initWithFormat:@"%@",[SelectedFilters objectForKey:@"status"]];
    
    _page_number=0;
    _search_word=@"";
}

-(void)GotList:(id)response
{
    NSLog(@"%@",response);
    NSString * status=[[NSString alloc]initWithFormat:@"%@",[response objectForKey:@"status"]];
    
    [KBSpinner dismiss];
    if ([status isEqualToString:@"1"])
    {
        NSMutableArray * tempArray=[response objectForKey:@"orderList"];
        
        NSString * notificationCount=[[NSString alloc]initWithFormat:@"%@",[response objectForKey:@"notificationCount"]];
        
        if (![notificationCount isEqualToString:@"0"])
        {
            [[[[[self tabBarController] tabBar] items]
              objectAtIndex:1] setBadgeValue:notificationCount];
        }
        else
        {
            [[[[[self tabBarController] tabBar] items]
              objectAtIndex:1] setBadgeValue:nil];
        }
        
        if (_page_number==0)
        {
            NSString * totalCount=[[NSString alloc]initWithFormat:@"%@",[response objectForKey:@"totalCount"]];
            
            _TotalCount=[totalCount integerValue];
            
            if (tempArray.count==0)
            {
                _labelEmpty.hidden=false;
            }
            else
            {
                _labelEmpty.hidden=true;
            }
        }
        
        for (int k=0;k<tempArray.count;k++)
        {
            [_tableArray addObject:tempArray[k]];
        }
        
        [_table reloadData];
    }
    else
    {
        
    }
}

-(void)ErrorResponse:(id)response
{
    NSLog(@"%@",response);
}

-(void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
    NSString * InternetAvailability=[Internet CheckInternet];
    if ([InternetAvailability isEqualToString:@"Yes"])
    {
        if (_TotalCount/10>_page_number)
        {
            _page_number++;
            
            [self CallApi];
        }
    }
    else
    {
        [KBAlert ShowInternetToast];
    }
}

-(void)CallApi
{
    [KBSpinner show];
    
    [KBAPIService OrderListAPI:_delivery_user_id :_from_date :_to_date :_order_status :_page_number :_search_word withDelegate:self onSuccessCallback:@selector(GotList:) onFailureCallback:@selector(ErrorResponse:)];
}

// tableview methods
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return _tableArray.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *simpleTableIdentifier = @"AdminNotificationTableViewCell";
    
    AdminNotificationTableViewCell *cell = (AdminNotificationTableViewCell *)[tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
    
    if (cell == nil)
    {
        NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"AdminNotificationTableViewCell" owner:self options:nil];
        cell = [nib objectAtIndex:0];
    }
    
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    NSMutableDictionary * oneDict=[_tableArray objectAtIndex:indexPath.row];
    
    cell.LabelId.text=[[NSString alloc]initWithFormat:@"ID : %@",[oneDict objectForKey:@"order_number"]];
    
    cell.labelPrice.text=[[NSString alloc]initWithFormat:@"%@",[oneDict objectForKey:@"total_price"]];
    
    cell.labelAddress.text=[[NSString alloc]initWithFormat:@"%@",[oneDict objectForKey:@"billing_address"]];
    
    NSString * assignedTo=[[NSString alloc]initWithFormat:@"%@",[oneDict objectForKey:@"delivery_person"]];
    
    if ([assignedTo isEqualToString:@""])
    {
        cell.labelDetails.text=@"Assigned To - None";
    }
    else{
        cell.labelDetails.text=[[NSString alloc]initWithFormat:@"Assigned To - %@",assignedTo];
    }
    
    [cell.BackView.layer setCornerRadius:10.0f];
    [cell.BackView.layer setShadowColor:[UIColor groupTableViewBackgroundColor].CGColor];
    [cell.BackView.layer setShadowOpacity:2.0];
    [cell.BackView.layer setShadowRadius:5.0];
    [cell.BackView.layer setShadowOffset:CGSizeMake(0.0f, 2.0f)];

    [cell.buttonAccept addTarget:self action:@selector(OrderApproved:) forControlEvents:UIControlEventTouchUpInside];
    cell.buttonAccept.tag=indexPath.row;
    
    [cell.buttonDecline addTarget:self action:@selector(OrderRejected:) forControlEvents:UIControlEventTouchUpInside];
    cell.buttonDecline.tag=indexPath.row;
    
    NSURL * FinalURL=[NSURL URLWithString:[oneDict objectForKey:@"order_image_icon"]];
    
    [cell.ImageProduct sd_setImageWithURL:FinalURL placeholderImage:[UIImage imageNamed:@"default_user_icon"] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
    }];
    
    cell.LABELORDEREDBY.text=[[NSString alloc]initWithFormat:@"Ordered By - %@",[oneDict objectForKey:@"customer_name"]];
    
    cell.ImageProduct.layer.cornerRadius=10;
    cell.ImageProduct.clipsToBounds=YES;
    
    cell.backViewForProductImage.layer.cornerRadius=10;
    cell.backViewForProductImage.clipsToBounds=YES;
    
    return cell;
}

-(void)CallApproveRejectAPI:(NSString *)order_id :(NSString *)approve_reject_status
{
    [KBSpinner show];
    [KBAPIService AdminOrderApprove:order_id :approve_reject_status WithDelegate:self onSuccessCallback:@selector(OrderApprovedRejected:) onFailureCallback:@selector(ErrorResponse:)];
}

-(void)OrderApprovedRejected:(id)response
{
    NSString * status=[[NSString alloc]initWithFormat:@"%@",[response objectForKey:@"status"]];
    [KBSpinner dismiss];
    if ([status isEqualToString:@"1"])
    {
        if ([_selectedOrderStatus isEqualToString:@"Approved"])
        {
            [KBAlert ShowToast:@"Order Approved successfully." :@""];
        }
        else{
            [KBAlert ShowToast:@"Order Declined successfully." :@""];
        }
        
        
        [self InitializeValues];
        
        [self CallApi];
    }
}

-(void)OrderApproved:(UIButton *)sender
{
    UIAlertController * alert=[UIAlertController alertControllerWithTitle:nil message:@"Approve Order?" preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction * okAction=[UIAlertAction actionWithTitle:@"Yes" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action)
    {
        self.selectedOrderStatus=@"Approved";
        NSMutableDictionary * oneDict=[self->_tableArray objectAtIndex:sender.tag];
        NSString * order_id=[[NSString alloc]initWithFormat:@"%@",[oneDict objectForKey:@"order_id"]];
        
        [self CallApproveRejectAPI:order_id :@"2" ];
    }];
    
    UIAlertAction * noAction=[UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleDefault handler:nil];
    
    [alert addAction:noAction];
    [alert addAction:okAction];
    
    [self presentViewController:alert animated:YES completion:nil];
}

-(void)OrderRejected:(UIButton *)sender
{
    UIAlertController * alert=[UIAlertController alertControllerWithTitle:nil message:@"Decline Order?" preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction * okAction=[UIAlertAction actionWithTitle:@"Yes" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action)
    {
        self.selectedOrderStatus=@"Declined";
        
        NSMutableDictionary * oneDict=[self->_tableArray objectAtIndex:sender.tag];
        NSString * order_id=[[NSString alloc]initWithFormat:@"%@",[oneDict objectForKey:@"order_id"]];
        
        [self CallApproveRejectAPI:order_id :@"3" ];
    }];
    
    UIAlertAction * noAction=[UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleDefault handler:nil];
    
    [alert addAction:noAction];
    [alert addAction:okAction];
    
    [self presentViewController:alert animated:YES completion:nil];
}

-(void)KeyboardUpMethod:(NSNotification *)notification
{
    CGFloat bottomPadding = 0;
    
    if (@available(iOS 11.0, *)) {
        UIWindow *window = UIApplication.sharedApplication.keyWindow;
        bottomPadding = window.safeAreaInsets.bottom;
    }
    
    NSDictionary* keyboardInfo = [notification userInfo];
    NSValue* keyboardFrameBegin = [keyboardInfo valueForKey:UIKeyboardFrameEndUserInfoKey];
    CGRect keyboardFrameBeginRect = [keyboardFrameBegin CGRectValue];
    
    [_table setFrame:CGRectMake(0, self.table.frame.origin.y, self.table.frame.size.width, _tableHeight-keyboardFrameBeginRect.size.height-bottomPadding)];
}

- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar
{
    [_SearchBar resignFirstResponder];
}

-(void)KeyboardDownMethod:(NSNotification *)notification
{
    [_table setFrame:CGRectMake(0, self.table.frame.origin.y, self.table.frame.size.width, _tableHeight)];
}

-(void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText
{
    _page_number=0;
    _search_word=[[NSString alloc]initWithFormat:@"%@",searchText];
    
    [_tableArray removeAllObjects];
    [_table reloadData];
    
    [self CallApi];
}

@end
