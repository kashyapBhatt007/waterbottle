//
//  AdminOrderFilterViewController.m
//  WaterBottle
//
//  Created by Kashyap Bhatt on 22/07/19.
//  Copyright © 2019 Kashyap. All rights reserved.
//

#import "AdminOrderFilterViewController.h"
#import "FilterTableViewCell.h"
#import "DeliveryUsersViewController.h"
#import "TitleNameTableViewCell.h"
#import "BottomButtonTableViewCell.h"
#import "StatusViewController.h"
#import "KBDateTimeHelper.h"

@interface AdminOrderFilterViewController ()

@end

@implementation AdminOrderFilterViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.

    [self InitializeValues];
    //FilterTableViewCell
}

-(void)viewWillAppear:(BOOL)animated{
    _table.hidden=true;
}

-(void)viewDidAppear:(BOOL)animated
{
    [_table reloadData];
    _table.hidden=false;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 6;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row==0)
    {
        static NSString *simpleTableIdentifier = @"TitleNameTableViewCell";
        
        TitleNameTableViewCell *cell = (TitleNameTableViewCell *)[tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
        
        if (cell == nil)
        {
            NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"TitleNameTableViewCell" owner:self options:nil];
            cell = [nib objectAtIndex:0];
        }
        
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        
        cell.labelTitle.text=@"Filter By";
        
        return cell;
    }
    else if (indexPath.row==5)
    {
        static NSString *simpleTableIdentifier = @"BottomButtonTableViewCell";
        
        BottomButtonTableViewCell *cell = (BottomButtonTableViewCell *)[tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
        
        if (cell == nil)
        {
            NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"BottomButtonTableViewCell" owner:self options:nil];
            cell = [nib objectAtIndex:0];
        }
        
        CAGradientLayer *gradient = [CAGradientLayer layer];
        
        gradient.frame = cell.backView.bounds;
        gradient.colors = [NSArray arrayWithObjects:
                           (id)[UIColor colorWithRed:52.0f/255.0f green:37.0f/255.0f blue:175.0f/255.0f alpha:1.0f].CGColor,
                           (id)[UIColor colorWithRed:3.0f/255.0f green:110.0f/255.0f blue:216.0f/255.0f alpha:1.0f].CGColor,
                           nil];
        
        [cell.backView.layer insertSublayer:gradient atIndex:0];
        
        cell.labelButtonName.text=@"Apply Filter";
        
        cell.backView.layer.cornerRadius=20;
        cell.backView.clipsToBounds=true;
        
        cell.selectionStyle=UITableViewCellSelectionStyleNone;
        return cell;
    }
    else{
        static NSString *simpleTableIdentifier = @"FilterTableViewCell";
        
        FilterTableViewCell *cell = (FilterTableViewCell *)[tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
        
        if (cell == nil)
        {
            NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"FilterTableViewCell" owner:self options:nil];
            cell = [nib objectAtIndex:0];
        }
        
        if (indexPath.row==1)
        {
            cell.label1.text=@"From Date";
            cell.labelValue.text=_fromDate;
            cell.imageAccessory.image=[UIImage imageNamed:@"calendar_icon"];
            cell.labelValue.text=_fromDate;
        }
        else if (indexPath.row==2)
        {
            cell.label1.text=@"To Date";
            cell.labelValue.text=_toDate;
            cell.imageAccessory.image=[UIImage imageNamed:@"calendar_icon"];
            cell.labelValue.text=_toDate;
        }
        else if (indexPath.row==3)
        {
            cell.label1.text=@"Status";
            cell.labelValue.text=_status;
            cell.imageAccessory.image=[UIImage imageNamed:@"down_arrow"];
            
            cell.labelValue.text=_status;
        }
        else if (indexPath.row==4)
        {
            cell.label1.text=@"Executive";
            cell.labelValue.text=_executive;
            cell.imageAccessory.image=[UIImage imageNamed:@"down_arrow"];
            
            cell.labelValue.text=_executive;
        }
        
        
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        
        return cell;
    }
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row==1)
    {
        _fromOrTo=[[NSString alloc]initWithFormat:@"%@",@"From"];
        MDDatePickerDialog *datePicker = [[MDDatePickerDialog alloc] init];
        datePicker.delegate = self;
        [datePicker show];
        // open date selection
    }
    else if (indexPath.row==2)
    {
        _fromOrTo=[[NSString alloc]initWithFormat:@"%@",@"To"];
        MDDatePickerDialog *datePicker = [[MDDatePickerDialog alloc] init];
        datePicker.delegate = self;
        [datePicker show];
        // open date selection
    }
    else if (indexPath.row==3)
    {
        // open status filter
        
        StatusViewController * StatusView = [[StatusViewController alloc]init];
        StatusView.delegate=self;
        StatusView.order_notification_payment=_order_notification_payment;
        [self presentViewController:StatusView animated:true completion:nil];
    }
    else if (indexPath.row==4)
    {
        DeliveryUsersViewController * DeliveryUsersView = [[DeliveryUsersViewController alloc]init];
        DeliveryUsersView.delegate=self;
        [self presentViewController:DeliveryUsersView animated:true completion:nil];
    }
    else if (indexPath.row==5)
    {
        NSMutableDictionary * FilterDictionary=[[NSMutableDictionary alloc]init];
        
        [FilterDictionary setObject:_fromDate forKey:@"fromDate"];
        if ([_fromDate isEqualToString:@"DD/MM/YY"])
        {
            [FilterDictionary setObject:@"" forKey:@"fromDate"];
        }
        
        [FilterDictionary setObject:_toDate forKey:@"toDate"];
        if ([_toDate isEqualToString:@"DD/MM/YY"])
        {
            [FilterDictionary setObject:@"" forKey:@"toDate"];
        }
        
        [FilterDictionary setObject:_statusId forKey:@"status"];
        [FilterDictionary setObject:_executiveId forKey:@"executive"];
        
        [self.navigationController popViewControllerAnimated:true];
        [self.delegate SelectedFilters:FilterDictionary];
    }
}

-(void)SelectedExecutive:(NSMutableDictionary *)SelectedExecutive
{
    _executive=[[NSString alloc]initWithFormat:@"%@",[SelectedExecutive objectForKey:@"name"]];
    _executiveId=[[NSString alloc]initWithFormat:@"%@",[SelectedExecutive objectForKey:@"user_id"]];
    [_table reloadData];
}

-(void)InitializeValues
{
    _fromDate=[[NSString alloc]initWithFormat:@"%@",@"DD/MM/YY"];
    _toDate=[[NSString alloc]initWithFormat:@"%@",@"DD/MM/YY"];
    _status=[[NSString alloc]initWithFormat:@"%@",@"Status"];
    _executive=[[NSString alloc]initWithFormat:@"%@",@"Executive"];
    _statusId=[[NSString alloc]initWithFormat:@"%@",@"0"];
    _executiveId=[[NSString alloc]initWithFormat:@"%@",@"0"];
}

- (IBAction)back:(id)sender
{
    [self.navigationController popViewControllerAnimated:true];
}

-(void)datePickerDialogDidSelectDate:(NSDate *)date
{
    if ([_fromOrTo isEqualToString:@"From"])
    {
        _fromDate=[KBDateTimeHelper ConvertDate:date ToFormat:@"yyyy-MM-dd"];
    }
    else
    {
        _toDate=[KBDateTimeHelper ConvertDate:date ToFormat:@"yyyy-MM-dd"];
    }
    
    [_table reloadData];
}

-(void)SelectedStatus:(NSMutableDictionary *)SelectedStatus
{
    _statusId=[[NSString alloc]initWithFormat:@"%@",[SelectedStatus objectForKey:@"id"]];
    _status=[[NSString alloc]initWithFormat:@"%@",[SelectedStatus objectForKey:@"name"]];
    
    [_table reloadData];
}
@end
