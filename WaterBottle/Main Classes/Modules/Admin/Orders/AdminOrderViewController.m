//
//  AdminOrderViewController.m
//  WaterBottle
//
//  Created by Kashyap Bhatt on 22/07/19.
//  Copyright © 2019 Kashyap. All rights reserved.
//

#import "AdminOrderViewController.h"
#import "AdminOrderTableViewCell.h"
#import "AdminOrderFilterViewController.h"
#import "KBAPIService.h"
#import "KBSpinner.h"
#import "Internet.h"
#import "KBAlert.h"
#import <QuartzCore/QuartzCore.h>
#import "DeliveryUsersViewController.h"
#import <SDWebImage/UIImageView+WebCache.h>

@interface AdminOrderViewController ()

@end

@implementation AdminOrderViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    _labelEmpty.hidden=true;
    
    [self InitializeValues];
}

-(void)viewDidAppear:(BOOL)animated
{
    _tableHeight=_table.frame.size.height;
    
    [_tableArray removeAllObjects];
    [_table reloadData];
    
    [self CallApi];
}

-(void)InitializeValues
{
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(KeyboardUpMethod:) name:UIKeyboardWillShowNotification object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(KeyboardDownMethod:) name:UIKeyboardWillHideNotification object:nil];
    
    _tableArray=[[NSMutableArray alloc]init];
    
    _delivery_user_id=[[NSString alloc]initWithFormat:@"%@",@"0"];
    _from_date=[[NSString alloc]initWithFormat:@"%@",@""];
    _to_date=[[NSString alloc]initWithFormat:@"%@",@""];
    _order_status=[[NSString alloc]initWithFormat:@"%@",@"0"];
    _page_number=0;
    _SearchBar.delegate=self;
    _search_word=[[NSString alloc]initWithFormat:@"%@",@""];
}

- (IBAction)GoToFilter:(id)sender
{
    AdminOrderFilterViewController * AdminOrderFilterView=[[AdminOrderFilterViewController alloc]init];
    AdminOrderFilterView.delegate=self;
    AdminOrderFilterView.order_notification_payment=[[NSString alloc]initWithFormat:@"%@",@"1"];
    [self.navigationController pushViewController:AdminOrderFilterView animated:true];
}

-(void)GotList:(id)response
{
    NSLog(@"%@",response);
    NSString * status=[[NSString alloc]initWithFormat:@"%@",[response objectForKey:@"status"]];
    
    [KBSpinner dismiss];
    if ([status isEqualToString:@"1"])
    {
        NSMutableArray * tempArray=[response objectForKey:@"orderList"];
        
        NSString * notificationCount=[[NSString alloc]initWithFormat:@"%@",[response objectForKey:@"notificationCount"]];
        
        if (![notificationCount isEqualToString:@"0"]){
            
            [[[[[self tabBarController] tabBar] items]
              objectAtIndex:1] setBadgeValue:notificationCount];
        }
        else{
            [[[[[self tabBarController] tabBar] items]
              objectAtIndex:1] setBadgeValue:nil];
        }
        
        if (_page_number==0)
        {
            NSString * totalCount=[[NSString alloc]initWithFormat:@"%@",[response objectForKey:@"totalCount"]];
            
            _TotalCount=[totalCount integerValue];
            
            if (tempArray.count==0)
            {
                _table.hidden=true;
                _labelEmpty.hidden=false;
            }
            else
            {
                _table.hidden=false;
                _labelEmpty.hidden=true;
            }
        }
        
        for (int k=0;k<tempArray.count;k++)
        {
            [_tableArray addObject:tempArray[k]];
        }
        
        [_table reloadData];
    }
    else
    {
        
    }
}

-(void)ErrorResponse:(id)response
{
    NSLog(@"%@",response);
}

-(void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
    NSString * InternetAvailability=[Internet CheckInternet];
    if ([InternetAvailability isEqualToString:@"Yes"])
    {
        if (_TotalCount/10>_page_number)
        {
            _page_number++;

            [self CallApi];
        }
    }
    else
    {
        [KBAlert ShowInternetToast];
    }
}

-(void)CallApi
{
    [KBSpinner show];
    
    [KBAPIService OrderListAPI:_delivery_user_id :_from_date :_to_date :_order_status :_page_number :_search_word withDelegate:self onSuccessCallback:@selector(GotList:) onFailureCallback:@selector(ErrorResponse:)];
}

// tableview methods
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return _tableArray.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *simpleTableIdentifier = @"AdminOrderTableViewCell";
    
    AdminOrderTableViewCell *cell = (AdminOrderTableViewCell *)[tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
    
    if (cell == nil)
    {
        NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"AdminOrderTableViewCell" owner:self options:nil];
        cell = [nib objectAtIndex:0];
    }
    
    CAGradientLayer *gradient = [CAGradientLayer layer];
    
    gradient.frame = cell.backStatusView.bounds;
    gradient.colors = [NSArray arrayWithObjects:
                       (id)[UIColor colorWithRed:52.0f/255.0f green:37.0f/255.0f blue:175.0f/255.0f alpha:1.0f].CGColor,
                       (id)[UIColor colorWithRed:3.0f/255.0f green:110.0f/255.0f blue:216.0f/255.0f alpha:1.0f].CGColor,
                       nil];
    
    [cell.backStatusView.layer insertSublayer:gradient atIndex:0];
    
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    NSMutableDictionary * oneDict=[_tableArray objectAtIndex:indexPath.row];
    
    cell.LabelId.text=[[NSString alloc]initWithFormat:@"ID : %@",[oneDict objectForKey:@"order_number"]];
    
    NSString * deliver_person=[[NSString alloc]initWithFormat:@"%@",[oneDict objectForKey:@"delivery_person"]];
    
    if ([deliver_person isEqualToString:@""])
    {
        [cell.buttonAssignedTo setTitle:@"None" forState:UIControlStateNormal];
    }
    else{
        [cell.buttonAssignedTo setTitle:deliver_person forState:UIControlStateNormal];
    }
    
    cell.labelPrice.text=[[NSString alloc]initWithFormat:@"%@",[oneDict objectForKey:@"total_price"]];
    
    cell.labelAddress.text=[[NSString alloc]initWithFormat:@"%@",[oneDict objectForKey:@"billing_address"]];
    
    NSString * completed_status=[[NSString alloc]initWithFormat:@"%@",[oneDict objectForKey:@"completed_status_title"]];
    
    cell.labelStatus.text=completed_status;
    
    cell.backStatusView.layer.cornerRadius=12.5f;
    cell.backStatusView.clipsToBounds=true;
    //ImageProduct;

    [cell.BackView.layer setCornerRadius:10.0f];
    [cell.BackView.layer setShadowColor:[UIColor groupTableViewBackgroundColor].CGColor];
    [cell.BackView.layer setShadowOpacity:2.0];
    [cell.BackView.layer setShadowRadius:5.0];
    [cell.BackView.layer setShadowOffset:CGSizeMake(0.0f, 2.0f)];
    
    cell.buttonAssigned.tag=indexPath.row;
    cell.buttonAssignedTo.tag=indexPath.row;
    
    [cell.buttonAssigned addTarget:self action:@selector(SetDeliveryGuy:) forControlEvents:UIControlEventTouchUpInside];
    [cell.buttonAssigned setTag:indexPath.row];
    [cell.buttonAssignedTo addTarget:self action:@selector(SetDeliveryGuy:) forControlEvents:UIControlEventTouchUpInside];
    [cell.buttonAssignedTo setTag:indexPath.row];
    
    cell.backViewForProduct.layer.cornerRadius=10;
    cell.backViewForProduct.clipsToBounds=YES;
    
    cell.ImageProduct.layer.cornerRadius=10;
    cell.ImageProduct.clipsToBounds=YES;
    
    NSURL * FinalURL=[NSURL URLWithString:[oneDict objectForKey:@"order_image_icon"]];
    
    [cell.ImageProduct sd_setImageWithURL:FinalURL placeholderImage:[UIImage imageNamed:@"default_user_icon"] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
    }];
    
    cell.labelOrderedBy.text=[[NSString alloc]initWithFormat:@"Ordered By - %@",[oneDict objectForKey:@"customer_name"]];
    
    return cell;
}

-(void)SetDeliveryGuy:(UIButton *)sender
{
    NSMutableDictionary * oneDict=[_tableArray objectAtIndex:sender.tag];

    _selectedOrderIdForDeliveryGuy=[[NSString alloc]initWithFormat:@"%@",[oneDict objectForKey:@"order_id"]];

    DeliveryUsersViewController * DeliveryUsersView=[[DeliveryUsersViewController alloc]init];
    DeliveryUsersView.modalPresentationStyle=UIModalPresentationOverCurrentContext;
    DeliveryUsersView.delegate=self;
    [self presentViewController:DeliveryUsersView animated:true completion:nil];
}

-(void)SelectedExecutive:(NSMutableDictionary *)SelectedExecutive
{
    NSString * user_id=[[NSString alloc]initWithFormat:@"%@",[SelectedExecutive objectForKey:@"user_id"]];
    
    [KBAPIService AssignDeliveryPerson:_selectedOrderIdForDeliveryGuy :user_id WithDelegate:self onSuccessCallback:@selector(DeliveryPersonAssigned:) onFailureCallback:@selector(ErrorResponse:)];
}

-(void)DeliveryPersonAssigned:(id)response
{
    NSLog(@"%@",response);
    
    NSString * status=[[NSString alloc]initWithFormat:@"%@",[response objectForKey:@"status"]];
    if ([status isEqualToString:@"1"])
    {
        [KBAlert ShowToast:[response objectForKey:@"message"] :@""];
        
        [self InitializeValues];
        
        [self CallApi];
    }
}


-(void)viewWillAppear:(BOOL)animated{
    [[self navigationController] setNavigationBarHidden:YES animated:YES];
}

-(void)KeyboardUpMethod:(NSNotification *)notification
{
    CGFloat bottomPadding = 0;
    
    if (@available(iOS 11.0, *)) {
        UIWindow *window = UIApplication.sharedApplication.keyWindow;
        bottomPadding = window.safeAreaInsets.bottom;
    }
    
    NSDictionary* keyboardInfo = [notification userInfo];
    NSValue* keyboardFrameBegin = [keyboardInfo valueForKey:UIKeyboardFrameEndUserInfoKey];
    CGRect keyboardFrameBeginRect = [keyboardFrameBegin CGRectValue];
    
    [_table setFrame:CGRectMake(0, self.table.frame.origin.y, self.table.frame.size.width, _tableHeight-keyboardFrameBeginRect.size.height-bottomPadding)];
}

- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar
{
    [_SearchBar resignFirstResponder];
}

-(void)KeyboardDownMethod:(NSNotification *)notification
{
    [_table setFrame:CGRectMake(0, self.table.frame.origin.y, self.table.frame.size.width, _tableHeight)];
}

-(void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText
{
    _page_number=0;
    _search_word=[[NSString alloc]initWithFormat:@"%@",searchText];
    
    [_tableArray removeAllObjects];
    [_table reloadData];
    
    [self CallApi];
}

-(void)SelectedFilters:(NSDictionary *)SelectedFilters
{
    [_tableArray removeAllObjects];
    [_table reloadData];
    
    _delivery_user_id=[[NSString alloc]initWithFormat:@"%@",[SelectedFilters objectForKey:@"executive"]];
    _from_date=[[NSString alloc]initWithFormat:@"%@",[SelectedFilters objectForKey:@"fromDate"]];
    _to_date=[[NSString alloc]initWithFormat:@"%@",[SelectedFilters objectForKey:@"toDate"]];
    _order_status=[[NSString alloc]initWithFormat:@"%@",[SelectedFilters objectForKey:@"status"]];
    
    _page_number=0;
    _search_word=@"";
}
@end
