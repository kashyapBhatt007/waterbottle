//
//  StatusViewController.m
//  WaterBottle
//
//  Created by Kashyap Bhatt on 17/08/19.
//  Copyright © 2019 Kashyap. All rights reserved.
//

#import "StatusViewController.h"
#import "OneLinerTableViewCell.h"
#import "KBAPIService.h"
#import "KBSpinner.h"
#import "Internet.h"
#import "KBAlert.h"

@interface StatusViewController ()

@end

@implementation StatusViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.

}

-(void)viewWillAppear:(BOOL)animated
{
    _labelEmpty.hidden=YES;
}

-(void)viewDidAppear:(BOOL)animated
{
    _tableHeight=_table.frame.size.height;
    
    [self InitializeValues];
    
    [self CallApi];
}

// tableview methods
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return _tableArray.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *simpleTableIdentifier = @"OneLinerTableViewCell";
    
    OneLinerTableViewCell *cell = (OneLinerTableViewCell *)[tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
    
    if (cell == nil)
    {
        NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"OneLinerTableViewCell" owner:self options:nil];
        cell = [nib objectAtIndex:0];
    }
    
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    NSMutableDictionary * oneDict=[_tableArray objectAtIndex:indexPath.row];
    cell.label1.text=[[NSString alloc]initWithFormat:@"%@",[oneDict objectForKey:@"name"]];
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSMutableDictionary * oneDict=[_tableArray objectAtIndex:indexPath.row];
    
    [self dismissViewControllerAnimated:YES completion:^{
        [self.delegate SelectedStatus:oneDict];
    }];
}

-(void)InitializeValues
{
    _tableArray=[[NSMutableArray alloc]init];
}

-(void)CallApi
{
    [KBSpinner show];
    
    [KBAPIService GetOrderPaymentStatus:_order_notification_payment WithDelegate:self onSuccessCallback:@selector(GotStatus:) onFailureCallback:@selector(ErrorResponse:)];
}

-(void)GotStatus:(id)response
{
    NSString * status=[[NSString alloc]initWithFormat:@"%@",[response objectForKey:@"status"]];
    
    [KBSpinner dismiss];
    
    if ([status isEqualToString:@"1"])
    {
        _tableArray=[response objectForKey:@"completionStatus"];
        
        if (_tableArray.count==0)
        {
            _labelEmpty.hidden=false;
        }
        else
        {
            _labelEmpty.hidden=true;
        }
        
        [_table reloadData];
    }
}

-(void)ErrorResponse:(id)response
{
    [KBSpinner dismiss];
    NSLog(@"%@",response);
}

- (IBAction)back:(id)sender
{
    [self dismissViewControllerAnimated:true completion:nil];
}

@end
