//
//  StatusViewController.h
//  WaterBottle
//
//  Created by Kashyap Bhatt on 17/08/19.
//  Copyright © 2019 Kashyap. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface StatusViewController : UIViewController

@property id delegate;

@property NSMutableArray * tableArray;
@property (weak, nonatomic) IBOutlet UILabel *labelEmpty;

@property int page_number;

@property NSString * search_word;
- (IBAction)back:(id)sender;

@property (weak, nonatomic) IBOutlet UITableView *table;
@property NSInteger TotalCount;
@property CGFloat tableHeight;

@property NSString * order_notification_payment;

@end

@protocol StatusViewControllerDelegate <NSObject>

-(void)SelectedStatus:(NSMutableDictionary *)SelectedStatus;

@end

NS_ASSUME_NONNULL_END
