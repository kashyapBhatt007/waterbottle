//
//  AdminOrderFilterViewController.h
//  WaterBottle
//
//  Created by Kashyap Bhatt on 22/07/19.
//  Copyright © 2019 Kashyap. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MDDatePickerDialog.h>

NS_ASSUME_NONNULL_BEGIN

@interface AdminOrderFilterViewController : UIViewController <UITableViewDelegate,UITableViewDataSource,MDDatePickerDialogDelegate>

- (IBAction)back:(id)sender;

@property id delegate;

@property (weak, nonatomic) IBOutlet UITableView *table;
@property NSString * fromDate;
@property NSString * toDate;
@property NSString * status;
@property NSString * statusId;
@property NSString * executive;
@property NSString * executiveId;
@property NSString * order_notification_payment;
@property NSString * fromOrTo;

@end

@protocol AdminOrderFilterViewControllerDelegate <NSObject>

-(void)SelectedFilters:(NSDictionary *)SelectedFilters;

@end
NS_ASSUME_NONNULL_END
