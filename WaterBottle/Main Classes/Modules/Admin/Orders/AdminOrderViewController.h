//
//  AdminOrderViewController.h
//  WaterBottle
//
//  Created by Kashyap Bhatt on 22/07/19.
//  Copyright © 2019 Kashyap. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface AdminOrderViewController : UIViewController <UITableViewDelegate,UITableViewDataSource,UISearchBarDelegate>

@property NSMutableArray * tableArray;

@property (weak, nonatomic) IBOutlet UILabel *labelEmpty;
@property (weak, nonatomic) IBOutlet UITableView *table;
- (IBAction)GoToFilter:(id)sender;
@property (weak, nonatomic) IBOutlet UISearchBar *SearchBar;
@property CGFloat tableHeight;
@property NSString * delivery_user_id;
@property NSString * from_date;
@property NSString * to_date;
@property NSString * order_status;
@property int page_number;
@property NSString * search_word;
@property NSString * selectedOrderIdForDeliveryGuy;
@property NSInteger TotalCount;

@end

NS_ASSUME_NONNULL_END
