//
//  DeliveryUsersViewController.h
//  WaterBottle
//
//  Created by Kashyap Bhatt on 23/07/19.
//  Copyright © 2019 Kashyap. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface DeliveryUsersViewController : UIViewController <UISearchBarDelegate,UINavigationControllerDelegate>

@property id delegate;

@property NSMutableArray * tableArray;
@property (weak, nonatomic) IBOutlet UILabel *labelEmpty;

@property int page_number;
- (IBAction)openSearchBar:(id)sender;
@property NSString * search_word;
- (IBAction)back:(id)sender;
@property (weak, nonatomic) IBOutlet UISearchBar *mySearchBar;
@property (weak, nonatomic) IBOutlet UITableView *table;
@property NSInteger TotalCount;
@property CGFloat tableHeight;
@end

@protocol DeliveryUsersViewControllerDelegate <NSObject>

-(void)SelectedExecutive:(NSMutableDictionary *)SelectedExecutive;

@end
NS_ASSUME_NONNULL_END
