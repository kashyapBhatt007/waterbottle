//
//  DeliveryUsersViewController.m
//  WaterBottle
//
//  Created by Kashyap Bhatt on 23/07/19.
//  Copyright © 2019 Kashyap. All rights reserved.
//

#import "DeliveryUsersViewController.h"
#import "OneLinerTableViewCell.h"
#import "KBAPIService.h"
#import "KBSpinner.h"
#import "Internet.h"
#import "KBAlert.h"

@interface DeliveryUsersViewController ()

@end

@implementation DeliveryUsersViewController


- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    _mySearchBar.autocorrectionType=UITextAutocorrectionTypeNo;
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(KeyboardUpMethod:) name:UIKeyboardWillShowNotification object:nil];
}

-(void)KeyboardUpMethod:(NSNotification *)notification
{
    CGFloat bottomPadding = 0;
    
    if (@available(iOS 11.0, *)) {
        UIWindow *window = UIApplication.sharedApplication.keyWindow;
        bottomPadding = window.safeAreaInsets.bottom;
    }

    NSDictionary* keyboardInfo = [notification userInfo];
    NSValue* keyboardFrameBegin = [keyboardInfo valueForKey:UIKeyboardFrameEndUserInfoKey];
    CGRect keyboardFrameBeginRect = [keyboardFrameBegin CGRectValue];
    
    [_table setFrame:CGRectMake(0, self.table.frame.origin.y, self.table.frame.size.width, _tableHeight-keyboardFrameBeginRect.size.height-bottomPadding)];
}

-(void)viewWillAppear:(BOOL)animated
{
    _labelEmpty.hidden=YES;
}

-(void)viewDidAppear:(BOOL)animated
{
    _tableHeight=_table.frame.size.height;
    
    [self InitializeValues];
    
    [self CallApi];
}

// tableview methods
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return _tableArray.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *simpleTableIdentifier = @"OneLinerTableViewCell";
    
    OneLinerTableViewCell *cell = (OneLinerTableViewCell *)[tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
    
    if (cell == nil)
    {
        NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"OneLinerTableViewCell" owner:self options:nil];
        cell = [nib objectAtIndex:0];
    }
    
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    NSMutableDictionary * oneDict=[_tableArray objectAtIndex:indexPath.row];
    cell.label1.text=[[NSString alloc]initWithFormat:@"%@",[oneDict objectForKey:@"name"]];
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSMutableDictionary * oneDict=[_tableArray objectAtIndex:indexPath.row];
    
    [self dismissViewControllerAnimated:YES completion:^{
        [self.delegate SelectedExecutive:oneDict];
    }];
}

-(void)InitializeValues
{
    _mySearchBar.delegate=self;
    _tableArray=[[NSMutableArray alloc]init];

    _page_number=0;
    _search_word=[[NSString alloc]initWithFormat:@"%@",@""];
}

-(void)CallApi
{
    [KBSpinner show];
    
    [KBAPIService GetDeliveryExecutives:_page_number :_search_word withDelegate:self onSuccessCallback:@selector(GotExecutives:) onFailureCallback:@selector(ErrorResponse:)];
}

-(void)GotExecutives:(id)response
{
    NSString * status=[[NSString alloc]initWithFormat:@"%@",[response objectForKey:@"status"]];
    
    [KBSpinner dismiss];
    
    if ([status isEqualToString:@"1"])
    {
        NSMutableArray * tempArray=[response objectForKey:@"deliveryUsers"];
        
        if (_page_number==0)
        {
            [_tableArray removeAllObjects];
            [_table reloadData];
            
            NSString * totalCount=[[NSString alloc]initWithFormat:@"%@",[response objectForKey:@"totalCount"]];
            
            _TotalCount=[totalCount integerValue];
            
            if (tempArray.count==0)
            {
                _labelEmpty.hidden=false;
            }
            else
            {
                _labelEmpty.hidden=true;
            }
        }
        
        for (int k=0;k<tempArray.count;k++)
        {
            [_tableArray addObject:tempArray[k]];
        }
        
        [_table reloadData];
        
        if (![_mySearchBar isFirstResponder])
        {
            [_mySearchBar becomeFirstResponder];
        }
    }
}

-(void)ErrorResponse:(id)response
{
    [KBSpinner dismiss];
    NSLog(@"%@",response);
}

-(void)searchBarCancelButtonClicked:(UISearchBar *)searchBar
{
    [self.view endEditing:YES];
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar
{
    
}

-(void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText
{
    _page_number=0;
    _search_word=[[NSString alloc]initWithFormat:@"%@",searchText];
    
    [self CallApi];
}

@end
