//
//  AdminNotificationTableViewCell.h
//  WaterBottle
//
//  Created by Kashyap Bhatt on 05/08/19.
//  Copyright © 2019 Kashyap. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface AdminNotificationTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *LABELORDEREDBY;

@property (weak, nonatomic) IBOutlet UIView *backViewForProductImage;
@property (weak, nonatomic) IBOutlet UIImageView *ImageProduct;
@property (weak, nonatomic) IBOutlet UILabel *LabelId;
@property (weak, nonatomic) IBOutlet UILabel *labelPrice;
@property (weak, nonatomic) IBOutlet UILabel *labelAddress;
@property (weak, nonatomic) IBOutlet UILabel *labelDetails;
@property (weak, nonatomic) IBOutlet UIView *BackView;
@property (weak, nonatomic) IBOutlet UIButton * buttonDecline;
@property (weak, nonatomic) IBOutlet UIButton * buttonAccept;
@end

NS_ASSUME_NONNULL_END
