//
//  BottomButtonTableViewCell.h
//  WaterBottle
//
//  Created by Kashyap Bhatt on 17/08/19.
//  Copyright © 2019 Kashyap. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface BottomButtonTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIView * backView;
@property (weak, nonatomic) IBOutlet UILabel * labelButtonName;
@end

NS_ASSUME_NONNULL_END
