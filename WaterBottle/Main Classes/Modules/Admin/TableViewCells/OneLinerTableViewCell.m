//
//  OneLinerTableViewCell.m
//  WaterBottle
//
//  Created by Kashyap Bhatt on 23/07/19.
//  Copyright © 2019 Kashyap. All rights reserved.
//

#import "OneLinerTableViewCell.h"

@implementation OneLinerTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
