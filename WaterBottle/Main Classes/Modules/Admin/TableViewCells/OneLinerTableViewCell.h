//
//  OneLinerTableViewCell.h
//  WaterBottle
//
//  Created by Kashyap Bhatt on 23/07/19.
//  Copyright © 2019 Kashyap. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface OneLinerTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel * label1;

@end

NS_ASSUME_NONNULL_END
