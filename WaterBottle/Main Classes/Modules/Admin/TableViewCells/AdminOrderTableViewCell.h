//
//  AdminOrderTableViewCell.h
//  WaterBottle
//
//  Created by Kashyap Bhatt on 22/07/19.
//  Copyright © 2019 Kashyap. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface AdminOrderTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIView *backViewForProduct;
@property (weak, nonatomic) IBOutlet UIButton *buttonAssigned;
@property (weak, nonatomic) IBOutlet UIImageView *ImageProduct;
@property (weak, nonatomic) IBOutlet UILabel *LabelId;
@property (weak, nonatomic) IBOutlet UIButton *buttonAssignedTo;
@property (weak, nonatomic) IBOutlet UILabel *labelPrice;
@property (weak, nonatomic) IBOutlet UILabel *labelAddress;
@property (weak, nonatomic) IBOutlet UILabel *labelStatus;
@property (weak, nonatomic) IBOutlet UIView *BackView;
@property (weak, nonatomic) IBOutlet UIView *backStatusView;
@property (weak, nonatomic) IBOutlet UILabel *labelOrderedBy;

@end

NS_ASSUME_NONNULL_END
