//
//  AdminProfileViewController.h
//  WaterBottle
//
//  Created by Kashyap Bhatt on 08/08/19.
//  Copyright © 2019 Kashyap. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface AdminProfileViewController : UIViewController
- (IBAction)Logout:(id)sender;

@end

NS_ASSUME_NONNULL_END
