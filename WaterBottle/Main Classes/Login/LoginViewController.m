//
//  LoginViewController.m
//  WaterBottle
//
//  Created by Ironman on 28/05/19.
//  Copyright © 2019 Kashyap. All rights reserved.
//

#import "LoginViewController.h"
#import "ForgotPasswordViewController.h"
#import "SignupViewController.h"
#import "internet.h"
#import "KBAlert.h"
#import "KBAPIService.h"
#import "KBSpinner.h"
#import "AdminTabBarController.h"
#import "CustomerTabBarViewController.h"
#import "DeliveryTabbarViewController.h"
#import "StaticKeys.h"

@interface LoginViewController ()

@end

@implementation LoginViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(KeyboardUpMethod:) name:UIKeyboardWillShowNotification object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(KeyboardDownMethod:) name:UIKeyboardWillHideNotification object:nil];
    
    _textPhoneNumber.delegate=self;
    
    _buttonSignin.layer.cornerRadius=25;
    _buttonSignin.clipsToBounds=YES;
    
    [[self navigationController] setNavigationBarHidden:YES animated:YES];
}

-(void)KeyboardUpMethod:(NSNotification *)notification
{
    [_backView setFrame:CGRectMake(0, self.backView.frame.origin.y-150, self.backView.frame.size.width, self.backView.frame.size.height)];
}

-(void)KeyboardDownMethod:(NSNotification *)notification
{
    [self MoveKeyboardDown];
}

-(void)viewDidAppear:(BOOL)animated{
    
    _yAxisForBackView=self.backView.frame.origin.y;
    
    UIColor * color = [UIColor whiteColor]; // select needed color
    
    NSString *string1 = @"Phone Number"; // the string to colorize
    NSDictionary *attrs1 = @{ NSForegroundColorAttributeName : color };
    NSAttributedString *attrStr1 = [[NSAttributedString alloc] initWithString:string1 attributes:attrs1];
    
    [_textPhoneNumber setTranslatesAutoresizingMaskIntoConstraints:YES];
    _textPhoneNumber.floatLabelActiveColor = [UIColor whiteColor];
    _textPhoneNumber.attributedPlaceholder=attrStr1;
    _textPhoneNumber.text = @"";
    _textPhoneNumber.keyboardType=UIKeyboardTypePhonePad;
    _textPhoneNumber.delegate = self;
    _textPhoneNumber.floatLabelFont=[UIFont systemFontOfSize:10.0f];
    _textPhoneNumber.font=[UIFont systemFontOfSize:16.0];
    _textPhoneNumber.autocorrectionType=UITextAutocorrectionTypeNo;
    
    CALayer *border = [CALayer layer];
    CGFloat borderWidth = 1;
    border.borderColor = [UIColor whiteColor].CGColor;
    border.frame = CGRectMake(0, _textPhoneNumber.frame.size.height - borderWidth, _textPhoneNumber.frame.size.width, _textPhoneNumber.frame.size.height);
    border.borderWidth = borderWidth;
    [_textPhoneNumber.layer addSublayer:border];
    _textPhoneNumber.layer.masksToBounds = YES;
    
    NSString *string = @"Password"; // the string to colorize
    NSDictionary *attrs = @{ NSForegroundColorAttributeName : color };
    NSAttributedString *attrStr = [[NSAttributedString alloc] initWithString:string attributes:attrs];
    
    [_textPassword setTranslatesAutoresizingMaskIntoConstraints:YES];
    _textPassword.floatLabelActiveColor = [UIColor whiteColor];
    _textPassword.attributedPlaceholder=attrStr;
    _textPassword.text = @"";
    _textPassword.keyboardType=UIKeyboardTypeEmailAddress;
    _textPassword.delegate = self;
    _textPassword.floatLabelFont=[UIFont systemFontOfSize:10.0f];
    _textPassword.font=[UIFont systemFontOfSize:16.0];
    _textPassword.autocorrectionType=UITextAutocorrectionTypeNo;
    
    CALayer *border1 = [CALayer layer];
    CGFloat borderWidth1 = 1;
    border1.borderColor = [UIColor whiteColor].CGColor;
    border1.frame = CGRectMake(0, _textPassword.frame.size.height - borderWidth1, _textPassword.frame.size.width, _textPassword.frame.size.height);
    border1.borderWidth = borderWidth1;
    [_textPassword.layer addSublayer:border1];
    _textPassword.layer.masksToBounds = YES;
}
- (IBAction)forgotPassword:(id)sender
{
    if ([_textPassword isFirstResponder] || [_textPhoneNumber isFirstResponder])
    {
        [self MoveKeyboardDown];
    }
    else
    {
        ForgotPasswordViewController * SignupView=[[ForgotPasswordViewController alloc]init];
        [self.navigationController pushViewController:SignupView animated:YES];
    }
}

- (IBAction)signUp:(id)sender
{
    if ([_textPassword isFirstResponder] || [_textPhoneNumber isFirstResponder])
    {
        [self MoveKeyboardDown];
    }
    else
    {
        SignupViewController * SignupView=[[SignupViewController alloc]init];
        [self.navigationController pushViewController:SignupView animated:YES];
    }
}

- (IBAction)signIn:(id)sender
{
    [UIView animateWithDuration:0.2f delay:0.0 options:UIViewAnimationOptionCurveEaseInOut
                     animations:^
     {
         [self->_backView setFrame:CGRectMake(self->_backView.frame.origin.x, self->_yAxisForBackView , self->_backView.frame.size.width, self->_backView.frame.size.height)];
         
     }           completion:^(BOOL finished) {
         [self.view endEditing:YES];
     }
     ];
    
    // call api
    
    NSString * phoneNumber=[[NSString alloc]initWithFormat:@"%@",_textPhoneNumber.text];
    NSString * password=[[NSString alloc]initWithFormat:@"%@",_textPassword.text];
    
    if ([phoneNumber isEqualToString:@""])
    {
        // show popup
    }
    else if ([password isEqualToString:@""])
    {
        // show popup
    }
    else
    {
        // check if internet is there or not
        NSString * InternetAvailability=[Internet CheckInternet];
        if ([InternetAvailability isEqualToString:@"Yes"])
        {
            [[UIApplication sharedApplication]beginIgnoringInteractionEvents];

            [KBSpinner show];
            
            [KBAPIService LoginWithPhoneNumber:phoneNumber Password:password withDelegate:self onSuccessCallback:@selector(LoginSuccess:) onFailureCallback:@selector(ErrorResponse:)];
        }
    }
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [self MoveKeyboardDown];
    
    return YES;
}

- (IBAction)tappedOnScreen:(id)sender
{
    [self MoveKeyboardDown];
}

-(void)LoginSuccess:(id)response
{
    NSString * status=[[NSString alloc]initWithFormat:@"%@",[response objectForKey:@"status"]];
    
    [KBSpinner dismiss];
    [[UIApplication sharedApplication]endIgnoringInteractionEvents];
    
    if ([status isEqualToString:@"1"])
    {
        NSLog(@"%@",response);
        
        NSUserDefaults * user=[[NSUserDefaults alloc]init];
        [user setObject:@"1" forKey:@"LoggedIn"];

        StaticKeys * staticKeysObject=[[StaticKeys alloc]init];
        
        NSString * auth_key=[[NSString alloc]initWithFormat:@"%@",[response objectForKey:@"auth_token"]];
        NSMutableDictionary * userDetails=[response objectForKey:@"userDetails"];
        
        NSString * country_code=[[NSString alloc]initWithFormat:@"%@",[userDetails objectForKey:@"country_code"]];
        NSString * email=[[NSString alloc]initWithFormat:@"%@",[userDetails objectForKey:@"email"]];
        NSString * user_id=[[NSString alloc]initWithFormat:@"%@",[userDetails objectForKey:@"id"]];
        NSString * name=[[NSString alloc]initWithFormat:@"%@",[userDetails objectForKey:@"name"]];
        NSString * phone=[[NSString alloc]initWithFormat:@"%@",[userDetails objectForKey:@"phone"]];
        NSString * user_role=[[NSString alloc]initWithFormat:@"%@",[userDetails objectForKey:@"user_role"]];
        
        [user setObject:auth_key forKey:staticKeysObject.auth_token];
        [user setObject:country_code forKey:staticKeysObject.country_code];
        [user setObject:email forKey:staticKeysObject.email];
        [user setObject:user_id forKey:staticKeysObject.user_id];
        [user setObject:name forKey:staticKeysObject.name];
        [user setObject:phone forKey:staticKeysObject.phone];
        [user setObject:user_role forKey:staticKeysObject.user_role];
        
        [self.view endEditing:YES];
        
        switch (user_role.integerValue) {
            case 1:
            {
                AdminTabBarController * MyOrdersView=[[AdminTabBarController alloc]init];
                [self.navigationController pushViewController:MyOrdersView animated:YES];
                break;
            }
                case 2:
            {
                DeliveryTabbarViewController * MyOrdersView=[[DeliveryTabbarViewController alloc]init];
                [self.navigationController pushViewController:MyOrdersView animated:YES];
                break;
            }
            case 3:
            {
                CustomerTabBarViewController * MyOrdersView=[[CustomerTabBarViewController alloc]init];
                [self.navigationController pushViewController:MyOrdersView animated:YES];
                break;
            }
                
            default:
                break;
        }
    }
    else
    {
        [KBAlert ShowToast:[response objectForKey:@"message"] :@"" ];
    }
}

-(void)ErrorResponse:(id)response
{
    [KBSpinner dismiss];
    [[UIApplication sharedApplication]endIgnoringInteractionEvents];
    NSLog(@"%@",response);
}



-(void)MoveKeyboardDown
{
    [UIView animateWithDuration:0.2f delay:0.0 options:UIViewAnimationOptionCurveEaseInOut
                     animations:^
     {
         [self->_backView setFrame:CGRectMake(self->_backView.frame.origin.x, self->_yAxisForBackView , self->_backView.frame.size.width, self->_backView.frame.size.height)];
         
     }           completion:^(BOOL finished) {
         [self.view endEditing:YES];
     }
     ];
}

@end
