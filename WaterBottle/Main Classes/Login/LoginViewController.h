//
//  LoginViewController.h
//  WaterBottle
//
//  Created by Ironman on 28/05/19.
//  Copyright © 2019 Kashyap. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <UIFloatLabelTextField/UIFloatLabelTextField.h>

NS_ASSUME_NONNULL_BEGIN

@interface LoginViewController : UIViewController <UITextFieldDelegate>

@property (weak, nonatomic) IBOutlet UIFloatLabelTextField *textPhoneNumber;

@property (weak, nonatomic) IBOutlet UIFloatLabelTextField *textPassword;
@property (weak, nonatomic) IBOutlet UIButton *buttonSignin;
- (IBAction)forgotPassword:(id)sender;
- (IBAction)signUp:(id)sender;

@property CGFloat yAxisForBackView;

- (IBAction)signIn:(id)sender;
@property (weak, nonatomic) IBOutlet UIView *backView;
- (IBAction)tappedOnScreen:(id)sender;

@end

NS_ASSUME_NONNULL_END
