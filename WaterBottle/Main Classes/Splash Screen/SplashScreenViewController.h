//
//  SplashScreenViewController.h
//  WaterBottle
//
//  Created by Kashyap Bhatt on 15/08/19.
//  Copyright © 2019 Kashyap. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface SplashScreenViewController : UIViewController

@end

NS_ASSUME_NONNULL_END
