//
//  SplashScreenViewController.m
//  WaterBottle
//
//  Created by Kashyap Bhatt on 15/08/19.
//  Copyright © 2019 Kashyap. All rights reserved.
//

#import "SplashScreenViewController.h"
#import "AdminTabBarController.h"
#import "CustomerTabBarViewController.h"
#import "DeliveryTabbarViewController.h"
#import "StaticKeys.h"
#import "LoginViewController.h"

@interface SplashScreenViewController ()

@end

@implementation SplashScreenViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

-(void)viewWillAppear:(BOOL)animated{
    NSUserDefaults * user=[[NSUserDefaults alloc]init];
    NSString * LoggedIn=[[NSString alloc]initWithFormat:@"%@",[user objectForKey:@"LoggedIn"]];
    
    if ([LoggedIn isEqualToString:@"1"])
    {
        NSUserDefaults * user=[[NSUserDefaults alloc]init];
        StaticKeys * StaticKeysObject=[[StaticKeys alloc]init];
        
        NSString * user_role=[[NSString alloc]initWithFormat:@"%@",[user objectForKey:StaticKeysObject.user_role]];
        
        switch (user_role.integerValue) {
            case 1:
            {
                AdminTabBarController * MyTabView=[[AdminTabBarController alloc]init];
                [self.navigationController pushViewController:MyTabView animated:true];
                break;
            }
            case 2:
            {
                DeliveryTabbarViewController * MyTabView=[[DeliveryTabbarViewController alloc]init];
                [self.navigationController pushViewController:MyTabView animated:true];
                break;
            }
            case 3:
            {
                CustomerTabBarViewController * MyTabView=[[CustomerTabBarViewController alloc]init];
                [self.navigationController pushViewController:MyTabView animated:true];
                break;
            }
                
            default:
                break;
        }
    }
    else
    {
        LoginViewController * MyTabView=[[LoginViewController alloc]init];
        [self.navigationController pushViewController:MyTabView animated:true];
    }
}

@end
