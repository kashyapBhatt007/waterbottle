//
//  ForgotPasswordViewController.m
//  WaterBottle
//
//  Created by Ironman on 28/05/19.
//  Copyright © 2019 Kashyap. All rights reserved.
//

#import "ForgotPasswordViewController.h"

@interface ForgotPasswordViewController ()

@end

@implementation ForgotPasswordViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
   
    // Do any additional setup after loading the view from its nib.
}

- (IBAction)back:(id)sender
{
    [self.view endEditing:true];
    
    [self.navigationController popViewControllerAnimated:YES];
}
@end
