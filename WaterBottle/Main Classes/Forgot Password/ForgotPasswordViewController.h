//
//  ForgotPasswordViewController.h
//  WaterBottle
//
//  Created by Ironman on 28/05/19.
//  Copyright © 2019 Kashyap. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface ForgotPasswordViewController : UIViewController
- (IBAction)back:(id)sender;

@end

NS_ASSUME_NONNULL_END
