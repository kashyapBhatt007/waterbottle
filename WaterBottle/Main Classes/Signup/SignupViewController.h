//
//  SignupViewController.h
//  WaterBottle
//
//  Created by Ironman on 28/05/19.
//  Copyright © 2019 Kashyap. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <UIFloatLabelTextField/UIFloatLabelTextField.h>

NS_ASSUME_NONNULL_BEGIN

@interface SignupViewController : UIViewController <UITextFieldDelegate,UITableViewDelegate>

- (IBAction)back:(id)sender;

@property CGFloat yAxisForBackView;
@property (weak, nonatomic) IBOutlet UIFloatLabelTextField *textName;
@property (weak, nonatomic) IBOutlet UIFloatLabelTextField *textPhoneNumber;
@property (weak, nonatomic) IBOutlet UIFloatLabelTextField *textmail;
@property (weak, nonatomic) IBOutlet UIFloatLabelTextField *textPassword;
@property (weak, nonatomic) IBOutlet UIFloatLabelTextField *textConfirmPassword;
@property (weak, nonatomic) IBOutlet UIView *backViewForTextFields;

- (IBAction)SignUp:(id)sender;
- (IBAction)goBackToSignIn:(id)sender;
@property (weak, nonatomic) IBOutlet UILabel *labelTerms;

- (IBAction)tappedOnScreen:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *buttonSignUp;
@end

NS_ASSUME_NONNULL_END
