//
//  SignupViewController.m
//  WaterBottle
//
//  Created by Ironman on 28/05/19.
//  Copyright © 2019 Kashyap. All rights reserved.
//

#import "SignupViewController.h"
#import "KBValidation.h"
#import "KBSpinner.h"
#import "KBAPIService.h"
#import "KBAlert.h"


@interface SignupViewController ()

@end

@implementation SignupViewController

-(void)viewWillAppear:(BOOL)animated
{
    _backViewForTextFields.hidden=YES;
}

-(void)viewDidAppear:(BOOL)animated
{
    [self SetupTextFields];
    
    CAGradientLayer *gradient = [CAGradientLayer layer];
    
    gradient.frame = _buttonSignUp.bounds;
    gradient.colors = [NSArray arrayWithObjects:
                       (id)[UIColor colorWithRed:52.0f/255.0f green:37.0f/255.0f blue:175.0f/255.0f alpha:1.0f].CGColor,
                       (id)[UIColor colorWithRed:3.0f/255.0f green:110.0f/255.0f blue:216.0f/255.0f alpha:1.0f].CGColor,
                       nil];
    
    [_buttonSignUp.layer insertSublayer:gradient atIndex:0];
    
    [_buttonSignUp.layer setCornerRadius:25];
    [_buttonSignUp setClipsToBounds:YES];
    
    self->_backViewForTextFields.hidden=NO;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    _labelTerms.text=@"By clicking Sign Up, you agree to our terms of \nservice & privacy policy";
}

- (IBAction)back:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)myNotificationMethod1:(NSNotification*)notification
{
    [_backViewForTextFields setFrame:CGRectMake(_backViewForTextFields.frame.origin.x, -100, _backViewForTextFields.frame.size.width, _backViewForTextFields.frame.size.height)];
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [_backViewForTextFields setFrame:CGRectMake(_backViewForTextFields.frame.origin.x, _yAxisForBackView, _backViewForTextFields.frame.size.width, _backViewForTextFields.frame.size.height)];
    
    [self.view endEditing:YES];
    return YES;
}

-(void)UserRegistered:(id)response
{
    NSLog(@"%@",response);
    [KBSpinner dismiss];
    
    [[UIApplication sharedApplication]endIgnoringInteractionEvents];
    
    NSString * status=[[NSString alloc]initWithFormat:@"%@",[response objectForKey:@"status"]];
    
    if ([status isEqualToString:@"1"])
    {
        [self.navigationController popViewControllerAnimated:true];
        [KBAlert ShowAlertWithTitle:@"Success" andMessage:@"Profile Created Successfully."];
    }
    else
    {
        [KBAlert ShowAlertWithTitle:@"Error !" andMessage:[response objectForKey:@"message"]];
    }
}

-(void)ErrorResponse:(id)response
{
    [KBSpinner dismiss];
    [[UIApplication sharedApplication]endIgnoringInteractionEvents];
    NSLog(@"%@",response);
}

- (IBAction)SignUp:(id)sender
{
    NSString * name=[[NSString alloc]initWithFormat:@"%@",_textName.text];
    
    NSString * phoneNumber=[[NSString alloc]initWithFormat:@"%@",_textPhoneNumber.text];
    
    NSString * email=[[NSString alloc]initWithFormat:@"%@",_textmail.text];
    
    NSString * password=[[NSString alloc]initWithFormat:@"%@",_textPassword.text];
    
    NSString * confirmPassword=[[NSString alloc]initWithFormat:@"%@",_textConfirmPassword.text];
    
    BOOL isEmailValid=[KBValidation NSStringIsValidEmail:email];
    
    if ([name isEqualToString:@""] || [phoneNumber isEqualToString:@""] || [email isEqualToString:@""] || [password isEqualToString:@""] || [confirmPassword isEqualToString:@""])
    {
        [KBAlert ShowToast:@"Please enter all values" :@""];
        // show alert for entering every value
    }
    else if (phoneNumber.length!=10)
    {
        [KBAlert ShowToast:@"Plese enter 10 digit number" :@""];
    }
    else if (isEmailValid==NO)
    {
        [KBAlert ShowToast:@"Please enter valid email" :@""];
        
        // show alert for email not valid
    }
    else if (![password isEqualToString:confirmPassword])
    {
        [KBAlert ShowToast:@"Password and Confirm Password should be same" :@""];
        
        // show that password and confirm password should be same
    }
    else
    {
        //        + (void)RegisterUser:(NSString *)name :(NSString *)email :(NSString *)country_code :(NSString *)phone :(NSString *)password
        
        [[UIApplication sharedApplication]beginIgnoringInteractionEvents];
        
        [KBSpinner show];
        
        [KBAPIService RegisterUser:name :email :@"+91" :phoneNumber :password withDelegate:self onSuccessCallback:@selector(UserRegistered:) onFailureCallback:@selector(ErrorResponse:)];
    }
}

- (IBAction)goBackToSignIn:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

-(void)SetupTextFields
{
    _textName.floatLabelActiveColor = [UIColor lightGrayColor];
    _textName.text = @"";
    _textName.delegate = self;
    _textName.placeholder=@"Name";
    _textName.keyboardType=UIKeyboardTypeDefault;
    _textName.floatLabelFont=[UIFont systemFontOfSize:10.0f];
    _textName.font=[UIFont systemFontOfSize:16.0];
    _textName.autocorrectionType=UITextAutocorrectionTypeNo;
    
    CALayer *border = [CALayer layer];
    CGFloat borderWidth = 1;
    border.borderColor = [UIColor darkGrayColor].CGColor;
    border.frame = CGRectMake(0, 50 - borderWidth, self.view.frame.size.width-60, 1);
    border.borderWidth = borderWidth;
    [_textName.layer addSublayer:border];
    _textName.layer.masksToBounds = YES;
    
    _textPhoneNumber.floatLabelActiveColor = [UIColor lightGrayColor];
    _textPhoneNumber.text = @"";
    _textPhoneNumber.keyboardType=UIKeyboardTypePhonePad;
    _textPhoneNumber.delegate = self;
    _textPhoneNumber.placeholder=@"Phone Number";
    _textPhoneNumber.floatLabelFont=[UIFont systemFontOfSize:10.0f];
    _textPhoneNumber.font=[UIFont systemFontOfSize:16.0];
    _textPhoneNumber.autocorrectionType=UITextAutocorrectionTypeNo;
    
    CALayer *border1 = [CALayer layer];
    CGFloat borderWidth1 = 1;
    border1.borderColor = [UIColor darkGrayColor].CGColor;
    border1.frame = CGRectMake(0, 50 - borderWidth1, self.view.frame.size.width-60, 1);
    border1.borderWidth = borderWidth1;
    [_textPhoneNumber.layer addSublayer:border1];
    _textPhoneNumber.layer.masksToBounds = YES;
    
    _textmail.floatLabelActiveColor = [UIColor lightGrayColor];
    _textmail.text = @"";
    _textmail.delegate = self;
    _textmail.placeholder=@"Email";
    _textmail.keyboardType=UIKeyboardTypeEmailAddress;
    _textmail.floatLabelFont=[UIFont systemFontOfSize:10.0f];
    _textmail.font=[UIFont systemFontOfSize:16.0];
    _textmail.autocorrectionType=UITextAutocorrectionTypeNo;
    
    CALayer *border2 = [CALayer layer];
    CGFloat borderWidth2 = 1;
    border2.borderColor = [UIColor darkGrayColor].CGColor;
    border2.frame = CGRectMake(0, 50 - borderWidth2, self.view.frame.size.width-60, 1);
    border2.borderWidth = borderWidth2;
    [_textmail.layer addSublayer:border2];
    _textmail.layer.masksToBounds = YES;
    
    _textPassword.floatLabelActiveColor = [UIColor lightGrayColor];
    _textPassword.text = @"";
    _textPassword.delegate = self;
    _textPassword.placeholder=@"Password";
    _textPassword.keyboardType=UIKeyboardTypeDefault;
    _textPassword.floatLabelFont=[UIFont systemFontOfSize:10.0f];
    _textPassword.font=[UIFont systemFontOfSize:16.0];
    _textPassword.autocorrectionType=UITextAutocorrectionTypeNo;
    
    CALayer *border3 = [CALayer layer];
    CGFloat borderWidth3 = 1;
    border3.borderColor = [UIColor darkGrayColor].CGColor;
    border3.frame = CGRectMake(0, 50 - borderWidth3, self.view.frame.size.width-60, 1);
    border3.borderWidth = borderWidth3;
    [_textPassword.layer addSublayer:border3];
    _textPassword.layer.masksToBounds = YES;
    
    _textConfirmPassword.floatLabelActiveColor = [UIColor lightGrayColor];
    _textConfirmPassword.text = @"";
    _textConfirmPassword.delegate = self;
    _textConfirmPassword.placeholder=@"Confirm Password";
    _textConfirmPassword.keyboardType=UIKeyboardTypeDefault;
    _textConfirmPassword.floatLabelFont=[UIFont systemFontOfSize:10.0f];
    _textConfirmPassword.font=[UIFont systemFontOfSize:16.0];
    _textConfirmPassword.autocorrectionType=UITextAutocorrectionTypeNo;
    
    CALayer *border4 = [CALayer layer];
    CGFloat borderWidth4 = 1;
    border4.borderColor = [UIColor darkGrayColor].CGColor;
    border4.frame = CGRectMake(0, 50 - borderWidth4, self.view.frame.size.width-60, 1);
    border4.borderWidth = borderWidth4;
    [_textConfirmPassword.layer addSublayer:border4];
    _textConfirmPassword.layer.masksToBounds = YES;
}

- (IBAction)tappedOnScreen:(id)sender
{
    [self.view endEditing:YES];
}
@end
